﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PortalesIGGA
{
    public class Notificacion
    {
        //Model
        public int IdProyecto { get; set; }
        public int IdPlantilla { get; set; }
        public int IdConfiguracionNotificacion { get; set; }
        public string Destinatarios { get; set; }
        public string TipoEnvio { get; set; }
        public string DatosJSON { get; set; }

        //Logic
        private static string endpoint = "http://sistemanotificaciones.azurewebsites.net/Odata/Notificaciones";
        public async Task<string> CrearNotificacion()
        {
            HttpClient httpClient = new HttpClient();
            string json = JsonConvert.SerializeObject(this);
            Trace.WriteLine(json);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = httpClient.PostAsync(endpoint, content).Result;
            return await response.Content.ReadAsStringAsync();
        }
    }
}