﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;



namespace PortalesIGGA
{

    /// <summary>
    /// By: Santiago Suarez Quiroz
    /// based on example from:
    /// https://ppolyzos.com/2016/02/07/upload-a-file-to-azure-blob-storage-using-web-api/
    /// </summary>
    public class AzureStorageMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        private readonly CloudBlobContainer _blobContainer;

        public AzureStorageMultipartFormDataStreamProvider(CloudBlobContainer blobContainer) : base("azure")
        {
            _blobContainer = blobContainer;
        }

        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            if (headers == null) throw new ArgumentNullException(nameof(headers));
            //if (this.FileData.Count() != 1) throw new InvalidOperationException("Numero incorrecto de archivos");
            Trace.WriteLine("FileData: " + this.FileData.Count());

            if (headers.ContentType != null)
            {

                Trace.WriteLine("ContentType: " + headers.ContentType.ToString());
                // Generate a new filename for every new blob
                string fileName = Guid.NewGuid().ToString() + "/";

                // For form data, Content-Disposition header is a requirement
                ContentDispositionHeaderValue contentDisposition = headers.ContentDisposition;
                if (contentDisposition != null)
                {
                    fileName += contentDisposition.FileName.Trim('"');
                }

                CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(fileName);
                if (headers.ContentType != null)
                {
                    // Set appropriate content type for your uploaded file
                    blob.Properties.ContentType = headers.ContentType.MediaType;
                }
                this.FileData.Add(new MultipartFileData(headers, blob.Name));
                return blob.OpenWrite();
            }
            else
            {
                return base.GetStream(parent, headers);
            }

        }
    }
}