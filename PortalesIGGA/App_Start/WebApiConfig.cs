﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using PortalesIGGA.Models;


namespace PortalesIGGA
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            //endpoints
            builder.EntitySet<Accion>("Acciones");
            builder.EntitySet<Carpeta>("Carpetas");
            builder.EntitySet<Clasificacion>("Clasificaciones");
            builder.EntitySet<Documento>("Documentos");
            builder.EntitySet<Empresa>("Empresas");
            builder.EntitySet<Estado>("Estados");
            builder.EntitySet<EstadoTarea>("EstadosTarea");
            builder.EntitySet<Contacto>("Contactos");
            builder.EntitySet<Contrato>("Contratos");
            builder.EntitySet<Pais>("Paises");
            builder.EntitySet<Usuario>("Usuarios");
            builder.EntitySet<Grupo>("Grupos");
            builder.EntitySet<Subgrupo>("Subgrupos");
            builder.EntitySet<Especialidad>("Especialidades");
            builder.EntitySet<Subespecialidad>("Subespecialidades");
            builder.EntitySet<Proyecto>("Proyectos");
            //builder.EntitySet<Subestacion>("Subestaciones");
            builder.EntitySet<Subestacion>("Subestaciones");
            builder.EntitySet<Tarea>("Tareas");
            builder.EntitySet<DocumentoEspecificacionAnexo>("DocumentosEspecificacionAnexo");            
            builder.EntitySet<UsuarioProyecto>("UsuariosProyecto");
            builder.EntitySet<UsuarioContrato>("UsuariosContrato");
            builder.EntitySet<Permiso>("Permisos");
            builder.EntitySet<Rol>("Roles");

            //security module
            builder.EntitySet<Recurso>("Recursos");
            builder.EntitySet<RoleByAction>("RolesByAction");
            
            builder.EntitySet<Entregable>("Entregables");
            builder.EntitySet<ControlDocumento>("ControlDocumentos");
            
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
