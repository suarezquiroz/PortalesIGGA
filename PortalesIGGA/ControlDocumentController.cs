﻿using PortalesIGGA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalesIGGA.Controllers
{
    public class ControlDocumentController : Controller
    {
        private IggaDBEntities db = new IggaDBEntities();
        //GET: ContolDocument/Control
        [HttpPost]
        public JsonResult Control(ControlDocumentosDt documento)
        {
            RespuestaDt resp = new RespuestaDt();
            DateTime now = DateTime.Now;
            switch (documento.Rol)
            {

                //PASO 1 - Registro, diseñador
                case 1:
                    var entrega = db.Entregable.Where(x => x.CodigoDocumento == documento.DocumentoNumeroReferenciaCliente).FirstOrDefault();//(documento.DocumentoNumeroReferenciaCliente);
                    documento.Estado = "En proceso";
                    documento.TiempoRevision = 8;
                    documento.NumeroRevision = 0;
                    documento.Titulo = entrega.Descripcion;
                    documento.TipoDocumento = entrega.TipoDocumento;
                    documento.FechaEntregaProgramada = entrega.FechaEntregaProgramada;
                    documento.Especialidad = entrega.Especialidad;
                    documento.ProyectoId = entrega.ProyectoId;
                    documento.MailRecibido = "PORTAL";
                    resp = CreatControlDocument(documento);
                    break;
                //PASO 2 Revisor //Actualización
                case 2:
                    var enga = db.Entregable.Where(x => x.CodigoDocumento == documento.DocumentoNumeroReferenciaCliente).FirstOrDefault();
                    documento.Estado = "Enviado";
                    documento.MailRecibido = "PORTAL";
                    documento.TiempoRevision = 8;
                    documento.NumeroRevision = 0;
                    documento.Titulo = enga.Descripcion;
                    documento.ProyectoId = enga.ProyectoId;
                    documento.TipoDocumento = enga.TipoDocumento;
                    documento.FechaEntregaProgramada = enga.FechaEntregaProgramada;
                    documento.Especialidad = enga.Especialidad;
                    documento.ClasificacionActual = documento.ClasificacionRevisor;
                    documento.FechaRespuestaRevisor = now;
                    resp = UpdateControlDocument(documento);
                    break;
                //validador // Actualización
                case 3:
                    var eg = db.Entregable.Where(x => x.CodigoDocumento == documento.DocumentoNumeroReferenciaCliente).FirstOrDefault();
                    
                    documento.Estado = "Enviado";
                    documento.MailRecibido = "PORTAL";
                    documento.TiempoRevision = 8;
                    documento.Revisor = eg.Revisor;
                    documento.NumeroRevision = 0;
                    documento.Titulo = eg.Descripcion;
                    documento.TipoDocumento = eg.TipoDocumento;
                    documento.FechaEntregaProgramada = eg.FechaEntregaProgramada;
                    documento.Especialidad = eg.Especialidad;
                    documento.ProyectoId = eg.ProyectoId;
                    documento.ClasificacionActual = documento.ClasificacionValidador;
                    documento.FechaRespuestaValidador = now;
                    resp = UpdateControlDocument(documento);
                    break;
                //default: 
                case 4:
                    var entr = db.Entregable.Where(x => x.CodigoDocumento == documento.DocumentoNumeroReferenciaCliente).FirstOrDefault();
                    documento.MailRecibido = "PORTAL";
                    documento.ProyectoId = entr.ProyectoId;
                    documento.Revisor = entr.Revisor;
                    documento.Titulo = entr.Descripcion;
                    documento.TipoDocumento = entr.TipoDocumento;
                    documento.FechaEntregaProgramada = entr.FechaEntregaProgramada;
                    documento.Especialidad = entr.Especialidad;
                    documento.FechaRecibidoDiseñador = now;
                    resp = UpdateControlDocument(documento);
                    break;
            }

            return Json(resp,JsonRequestBehavior.AllowGet);
        }

        private int tiempoRevisión(DateTime oldDate)
        {
            DateTime newDate = DateTime.Now;

            // Difference in days, hours, and minutes.
            TimeSpan ts = newDate - oldDate;

            // Difference in days.
            int differenceInDays = ts.Days;
            return differenceInDays;
        }

        private RespuestaDt UpdateControlDocument(ControlDocumentosDt document)
        {
            RespuestaDt resp = new RespuestaDt();
            try
            {
                ControlDocumento ControlDocumento = new ControlDocumento();
                ControlDocumento = mapeo(document);
                var entity = db.ControlDocumento.Find(document.Id);

                if (document.Rol == 1)
                {
                    entity.ClasificacionActual = document.ClasificacionActual;
                    entity.Estado = document.Estado;
                    entity.Revisor = document.Revisor;
                }

                if (document.Rol == 4)
                {
                    ControlDocumento.NumeroRevision = entity.NumeroRevision + 1;
                    ControlDocumento.FechaRespuestaRevisor = entity.FechaRespuestaRevisor;
                    ControlDocumento.ClasificacionRevisor = entity.ClasificacionRevisor;
                    ControlDocumento.ClasificacionValidador = entity.ClasificacionValidador;
                    ControlDocumento.FechaRespuestaValidador = entity.FechaRespuestaValidador;
                    //ControlDocumento.FechaRecibidoDiseñador = document.FechaRecibidoDiseñador;
                    //ControlDocumento.NumeroRevision = ControlDocumento.NumeroRevision;
                    entity.NumeroRevision = ControlDocumento.NumeroRevision;
                    entity.FechaRecibidoDisenador = document.FechaRecibidoDiseñador;
                    ControlDocumento.Estado = entity.Estado;
                    ControlDocumento.ClasificacionActual = entity.ClasificacionActual;
                }
                

                if (document.Rol == 2)
                {
                    entity.Estado = document.Estado;
                    entity.FechaRespuestaRevisor = document.FechaRespuestaRevisor;
                    entity.ClasificacionRevisor = document.ClasificacionRevisor;
                }
                if (document.Rol == 3)
                {
                    entity.Estado = document.Estado;
                    entity.ClasificacionValidador = document.ClasificacionValidador;
                    entity.FechaRespuestaValidador = document.FechaRespuestaValidador;
                    ControlDocumento.ClasificacionRevisor = entity.ClasificacionRevisor;
                    ControlDocumento.FechaRespuestaRevisor = entity.FechaRespuestaRevisor;
                }
                
                entity.ProyectoId = document.ProyectoId;
                db.Entry(entity).CurrentValues.SetValues(ControlDocumento);
                db.SaveChanges();

                resp.TipoResultado = 1;
                resp.TipoResultadoM = "Correcto";
                resp.Mensaje = "Se actualizo correctamente";
                return resp;
            }
            catch (Exception ex)
            {

                resp.TipoResultado = 2;
                resp.TipoResultadoM = "Error";
                resp.Mensaje = ex.Message;
                return resp;
            }


        }

        private RespuestaDt CreatControlDocument(ControlDocumentosDt document)
        {
            RespuestaDt resp = new RespuestaDt();
            try
            {
                ControlDocumento ControlDocumento = new ControlDocumento();
                ControlDocumento = mapeo(document);
                db.ControlDocumento.Add(ControlDocumento);
                db.SaveChanges();
                int id = ControlDocumento.Id;
                resp.TipoResultado = 1;
                resp.TipoResultadoM = "Correcto";
                resp.Mensaje = Convert.ToString(id);
                resp.Mensaje = "Se guarda correctamente";

                return resp;
            }
            catch (Exception ex)
            {
                resp.TipoResultado = 2;
                resp.TipoResultadoM = "Error";
                resp.Mensaje = ex.Message;
                return resp;
            }

        }

        private ControlDocumento mapeo(ControlDocumentosDt document)
        {
            ControlDocumento obj = new ControlDocumento();
            if (document.Id != null)
            {
                obj.Id = (document.Id != null) ? document.Id.Value : 0; 
            }
            obj.ProyectoId = document.ProyectoId;
            obj.ClasificacionActual = document.ClasificacionActual;
            obj.Estado = document.Estado;
            obj.TiempoRevision = document.TiempoRevision;
            obj.NumeroRevision = document.NumeroRevision;
            obj.DocumentoNumeroReferenciaCliente = document.DocumentoNumeroReferenciaCliente;
            obj.Titulo = document.Titulo;
            obj.TipoDocumento = document.TipoDocumento;
            obj.PrecioDocumento = document.PrecioDocumento;
            obj.ValorAFacturar = document.ValorAFacturar;
            obj.ValorPendienteXFacturar = document.ValorPendienteXFacturar;
            obj.FechaEntregaProgramada = document.FechaEntregaProgramada;
            obj.Revisor = document.Revisor;
            obj.Especialidad = document.Especialidad;
            obj.MailRecibido = document.MailRecibido;
            obj.FechaRecibidoDisenador = document.FechaRecibidoDiseñador;
            obj.FechaRespuestaRevisor = document.FechaRespuestaRevisor;
            obj.ClasificacionRevisor = document.ClasificacionRevisor;
            obj.EmailRevisor = document.EmailRevisor;
            obj.FechaRespuestaValidador = document.FechaRespuestaValidador;
            obj.ClasificacionValidador = document.ClasificacionValidador;

            return obj;
        }

        //// GET: ControlDocument
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //// GET: ControlDocument/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: ControlDocument/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: ControlDocument/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ControlDocument/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: ControlDocument/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ControlDocument/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: ControlDocument/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
