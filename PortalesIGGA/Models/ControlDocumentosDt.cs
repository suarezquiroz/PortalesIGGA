﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalesIGGA.Models
{
    public class ControlDocumentosDt
    {

        public Nullable<int> Id { get; set; }

        public Nullable<int> ProyectoId { get; set; }

        public string ClasificacionActual { get; set; }

        public string Estado { get; set; }

        public Nullable<decimal> TiempoRevision { get; set; }

        public Nullable<decimal> NumeroRevision { get; set; }

        public string DocumentoNumeroReferenciaCliente { get; set; }

        public string Titulo { get; set; }

        public string TipoDocumento { get; set; }

        public Nullable<decimal> PrecioDocumento { get; set; }

        public string ValorAFacturar { get; set; }

        public string ValorPendienteXFacturar { get; set; }

        public Nullable<System.DateTime> FechaEntregaProgramada { get; set; }

        public string Revisor { get; set; }

        public string Especialidad { get; set; }

        public string MailRecibido { get; set; }

        public Nullable<System.DateTime> FechaRecibidoDiseñador { get; set; }

        public Nullable<System.DateTime> FechaRespuestaRevisor { get; set; }

        public string ClasificacionRevisor { get; set; }

        public string EmailRevisor { get; set; }

        public Nullable<System.DateTime> FechaRespuestaValidador { get; set; }

        public string ClasificacionValidador { get; set; }

        public int Rol { get; set; }
    }
}