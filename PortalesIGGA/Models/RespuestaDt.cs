﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalesIGGA.Models
{
    public class RespuestaDt
    {
        public int TipoResultado { get; set; }
        public string TipoResultadoM { get; set; }
        public string Mensaje { get; set; }
        public int identificador { get; set; }
    }
}