﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<RoleByAction>("RolesByAction");
    builder.EntitySet<Accion>("Accion"); 
    builder.EntitySet<Rol>("Rol"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RolesByActionController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/RolesByAction
        [EnableQuery]
        public IQueryable<RoleByAction> GetRolesByAction()
        {
            return db.RoleByAction;
        }

        // GET: odata/RolesByAction(5)
        [EnableQuery]
        public SingleResult<RoleByAction> GetRoleByAction([FromODataUri] int key)
        {
            return SingleResult.Create(db.RoleByAction.Where(roleByAction => roleByAction.IdRolByAction == key));
        }

        // PUT: odata/RolesByAction(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<RoleByAction> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoleByAction roleByAction = db.RoleByAction.Find(key);
            if (roleByAction == null)
            {
                return NotFound();
            }

            patch.Put(roleByAction);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleByActionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(roleByAction);
        }

        // POST: odata/RolesByAction
        public IHttpActionResult Post(RoleByAction roleByAction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RoleByAction.Add(roleByAction);
            db.SaveChanges();

            return Created(roleByAction);
        }

        // PATCH: odata/RolesByAction(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<RoleByAction> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RoleByAction roleByAction = db.RoleByAction.Find(key);
            if (roleByAction == null)
            {
                return NotFound();
            }

            patch.Patch(roleByAction);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleByActionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(roleByAction);
        }

        // DELETE: odata/RolesByAction(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            RoleByAction roleByAction = db.RoleByAction.Find(key);
            if (roleByAction == null)
            {
                return NotFound();
            }

            db.RoleByAction.Remove(roleByAction);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/RolesByAction(5)/Accion
        [EnableQuery]
        public SingleResult<Accion> GetAccion([FromODataUri] int key)
        {
            return SingleResult.Create(db.RoleByAction.Where(m => m.IdRolByAction == key).Select(m => m.Accion));
        }

        // GET: odata/RolesByAction(5)/Rol
        [EnableQuery]
        public SingleResult<Rol> GetRol([FromODataUri] int key)
        {
            return SingleResult.Create(db.RoleByAction.Where(m => m.IdRolByAction == key).Select(m => m.Rol));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleByActionExists(int key)
        {
            return db.RoleByAction.Count(e => e.IdRolByAction == key) > 0;
        }
    }
}
