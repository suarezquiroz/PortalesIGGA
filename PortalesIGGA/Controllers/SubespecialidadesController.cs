﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Subespecialidad>("Subespecialidades");
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<Especialidad>("Especialidad"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SubespecialidadesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Subespecialidades
        [EnableQuery]
        public IQueryable<Subespecialidad> GetSubespecialidades()
        {
            return db.Subespecialidad;
        }

        // GET: odata/Subespecialidades(5)
        [EnableQuery]
        public SingleResult<Subespecialidad> GetSubespecialidad([FromODataUri] int key)
        {
            return SingleResult.Create(db.Subespecialidad.Where(subespecialidad => subespecialidad.Id == key));
        }

        // PUT: odata/Subespecialidades(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Subespecialidad> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subespecialidad subespecialidad = db.Subespecialidad.Find(key);
            if (subespecialidad == null)
            {
                return NotFound();
            }

            patch.Put(subespecialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubespecialidadExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subespecialidad);
        }

        // POST: odata/Subespecialidades
        public IHttpActionResult Post(Subespecialidad subespecialidad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Subespecialidad.Add(subespecialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SubespecialidadExists(subespecialidad.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(subespecialidad);
        }

        // PATCH: odata/Subespecialidades(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Subespecialidad> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subespecialidad subespecialidad = db.Subespecialidad.Find(key);
            if (subespecialidad == null)
            {
                return NotFound();
            }

            patch.Patch(subespecialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubespecialidadExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subespecialidad);
        }

        // DELETE: odata/Subespecialidades(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Subespecialidad subespecialidad = db.Subespecialidad.Find(key);
            if (subespecialidad == null)
            {
                return NotFound();
            }

            db.Subespecialidad.Remove(subespecialidad);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Subespecialidades(5)/Especialidad
        [EnableQuery]
        public SingleResult<Especialidad> GetEspecialidad([FromODataUri] int key)
        {
            return SingleResult.Create(db.Subespecialidad.Where(m => m.Id == key).Select(m => m.Especialidad));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubespecialidadExists(int key)
        {
            return db.Subespecialidad.Count(e => e.Id == key) > 0;
        }
    }
}
