﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Tarea>("Tareas");
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<EstadoTarea>("EstadoTarea"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class TareasController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Tareas
        [EnableQuery]
        public IQueryable<Tarea> GetTareas()
        {
            return db.Tarea;
        }

        // GET: odata/Tareas(5)
        [EnableQuery]
        public SingleResult<Tarea> GetTarea([FromODataUri] int key)
        {
            return SingleResult.Create(db.Tarea.Where(tarea => tarea.Id == key));
        }

        // PUT: odata/Tareas(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Tarea> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Tarea tarea = db.Tarea.Find(key);
            if (tarea == null)
            {
                return NotFound();
            }

            patch.Put(tarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TareaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(tarea);
        }

        // POST: odata/Tareas
        public IHttpActionResult Post(Tarea tarea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Tarea.Add(tarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TareaExists(tarea.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(tarea);
        }

        // PATCH: odata/Tareas(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Tarea> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Tarea tarea = db.Tarea.Find(key);
            if (tarea == null)
            {
                return NotFound();
            }

            patch.Patch(tarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TareaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(tarea);
        }

        // DELETE: odata/Tareas(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Tarea tarea = db.Tarea.Find(key);
            if (tarea == null)
            {
                return NotFound();
            }

            db.Tarea.Remove(tarea);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Tareas(5)/Documento
        [EnableQuery]
        public SingleResult<Documento> GetDocumento([FromODataUri] int key)
        {
            return SingleResult.Create(db.Tarea.Where(m => m.Id == key).Select(m => m.Documento));
        }

        // GET: odata/Tareas(5)/EstadoTarea
        [EnableQuery]
        public SingleResult<EstadoTarea> GetEstadoTarea([FromODataUri] int key)
        {
            return SingleResult.Create(db.Tarea.Where(m => m.Id == key).Select(m => m.EstadoTarea));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TareaExists(int key)
        {
            return db.Tarea.Count(e => e.Id == key) > 0;
        }
    }
}
