﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Grupo>("Grupos");
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<Subgrupo>("Subgrupo"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class GruposController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Grupos
        [EnableQuery]
        public IQueryable<Grupo> GetGrupos()
        {
            return db.Grupo;
        }

        // GET: odata/Grupos(5)
        [EnableQuery]
        public SingleResult<Grupo> GetGrupo([FromODataUri] int key)
        {
            return SingleResult.Create(db.Grupo.Where(grupo => grupo.Id == key));
        }

        // PUT: odata/Grupos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Grupo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Grupo grupo = db.Grupo.Find(key);
            if (grupo == null)
            {
                return NotFound();
            }

            patch.Put(grupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GrupoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(grupo);
        }

        // POST: odata/Grupos
        public IHttpActionResult Post(Grupo grupo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Grupo.Add(grupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (GrupoExists(grupo.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(grupo);
        }

        // PATCH: odata/Grupos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Grupo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Grupo grupo = db.Grupo.Find(key);
            if (grupo == null)
            {
                return NotFound();
            }

            patch.Patch(grupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GrupoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(grupo);
        }

        // DELETE: odata/Grupos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Grupo grupo = db.Grupo.Find(key);
            if (grupo == null)
            {
                return NotFound();
            }

            db.Grupo.Remove(grupo);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GrupoExists(int key)
        {
            return db.Grupo.Count(e => e.Id == key) > 0;
        }
    }
}
