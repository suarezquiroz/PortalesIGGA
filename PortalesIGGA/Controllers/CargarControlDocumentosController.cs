﻿using Newtonsoft.Json;
using PortalesIGGA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/CargarControlDocumentos")]
    public class CargarControlDocumentosController : ApiController
    {
        private IggaDBEntities db = new IggaDBEntities();

        //// GET: api/ControlDocumento
        [HttpGet, Route("")]
        public IQueryable<ControlDocumento> Get()
        {
            return db.ControlDocumento;
        }

        // GET: api/ControlDocumento/5
        [HttpGet, Route("{id:int}")]
        public ControlDocumento Get(int id)
        {
            return db.ControlDocumento.Where(control => control.Id == id).FirstOrDefault();
        }

        // POST: api/CargaEntregables
        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            string jsonString = await request.Content.ReadAsStringAsync();

            // Do something with the string 
            CargaControlDocumentos controlDocumentosJson = JsonConvert.DeserializeObject<CargaControlDocumentos>(jsonString);
            StringBuilder responseContent = new StringBuilder();


            db.ControlDocumento.AddRange(controlDocumentosJson.DatosExcel);
            //foreach (ControlDocumento controlDocumento in controlDocumentosJson.DatosExcel)
            //{
            //    string line = "{";
            //    foreach (var column in controlDocumento.GetType().GetProperties())
            //    {
            //        line += (string.Format("{0}: {1}, ", column.Name, column.GetValue(controlDocumento)?.ToString()));
            //    }
            //    line += "},";
            //    responseContent.AppendLine(line);
            //}

            try
            {

                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder exception = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    string entityError = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Trace.WriteLine(entityError);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        string validationError = string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                            ve.PropertyName,
                                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                            ve.ErrorMessage);
                        entityError += validationError;
                        Trace.WriteLine(validationError);

                    }
                    exception.AppendLine(entityError);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new StringContent(exception.ToString());
                throw new HttpResponseException(resp);
            }
            var response = Request.CreateResponse(HttpStatusCode.Created);
            //response.Content = new StringContent(responseContent.ToString());
            return response;
        }
    }
}

internal class CargaControlDocumentos
{
    public int ProyectoId { get; set; }
    public List<ControlDocumento> DatosExcel { get; set; }
}