﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using PortalesIGGA.Models;
using System.Data;
using System.Text;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Reflection;

namespace PortalesIGGA.Controllers
{

    internal class CargaEntregables
    {
        public int ProyectoId { get; set; }
        public List<Entregable> DatosExcel { get; set; }
    }

    [RoutePrefix("api/CargaEntregables")]
    public class CargaEntregablesController : ApiController
    {

        private IggaDBEntities db = new IggaDBEntities();

        //// GET: api/CargaEntregables
        [HttpGet, Route("")]
        public IQueryable<Entregable> Get()
        {
            return db.Entregable;
        }

        // GET: api/CargaEntregables/5
        [HttpGet, Route("{id:int}")]
        public Entregable Get(int id)
        {
            return db.Entregable.Where(entregable => entregable.Id == id).FirstOrDefault();
        }

        // POST: api/CargaEntregables
        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            string jsonString = await request.Content.ReadAsStringAsync();

            // Do something with the string 
            CargaEntregables entregablesJson = JsonConvert.DeserializeObject<CargaEntregables>(jsonString);
            //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

            foreach (Entregable entregable in entregablesJson.DatosExcel)
            {

                entregable.ProyectoId = entregablesJson.ProyectoId;

                Grupo grupo = db.Grupo.FirstOrDefault(g => g.Nombre.ToLower() == entregable.Grupo.ToLower());
                if (grupo == null || grupo.Id <= 0)
                {
                    grupo = db.Grupo.Add(
                        new Grupo
                        {
                            Nombre = entregable.Grupo,
                            Activo = true,
                            Descripcion = "Generado en la carga de documentos entregables"
                        }
                    );
                }
                db.SaveChanges();

                Subgrupo subgrupo = db.Subgrupo.FirstOrDefault(s => s.Nombre.ToLower() == entregable.Subgrupo.ToLower());
                if (subgrupo != null && subgrupo.Id > 0)
                {
                    subgrupo.Nombre = entregable.Subgrupo;
                    subgrupo.GrupoId = grupo.Id;
                    subgrupo.Activo = true;
                    subgrupo.Descripcion = "Generado en la carga de documentos entregables";
                }
                else
                {
                    db.Subgrupo.Add(
                        new Subgrupo
                        {
                            Nombre = entregable.Subgrupo,
                            GrupoId = grupo.Id,
                            Activo = true,
                            Descripcion = "Generado en la carga de documentos entregables"
                        }
                    );

                }
                db.SaveChanges();

                Especialidad especialidad = db.Especialidad.FirstOrDefault(e => e.Nombre.ToLower() == entregable.Especialidad.ToLower());

                if (especialidad == null || especialidad.Id <= 0)
                {
                    especialidad = db.Especialidad.Add(
                        new Especialidad
                        {
                            Nombre = entregable.Especialidad,
                            Activo = true,
                            Descripcion = "Generado en la carga de documentos entregables"
                        }
                    );
                }
                db.SaveChanges();

                Subespecialidad subespecialidad = db.Subespecialidad.FirstOrDefault(s => s.Nombre.ToLower() == entregable.Subespecialidad.ToLower());
                if (subespecialidad != null && subespecialidad.Id > 0)
                {

                    subespecialidad.Nombre = entregable.Subespecialidad;
                    subespecialidad.EspecialidadId = especialidad.Id;
                    subespecialidad.Activo = true;
                    subespecialidad.Descripcion = "Generado en la carga de documentos entregables";

                }
                else
                {
                    db.Subespecialidad.Add(
                        new Subespecialidad
                        {
                            Nombre = entregable.Subespecialidad,
                            EspecialidadId = especialidad.Id,
                            Activo = true,
                            Descripcion = "Generado en la carga de documentos entregables"
                        }
                    );
                }
                db.SaveChanges();

                Entregable _entregable = db.Entregable.FirstOrDefault(e => (e.CodigoDocumento.ToLower() == entregable.CodigoDocumento.ToLower() && e.ProyectoId.Value == entregablesJson.ProyectoId));

                if (_entregable != null)
                {
                    foreach (PropertyInfo property in typeof(Entregable).GetProperties().ToList())
                    {
                        if (property.GetValue(entregable) != null && property.Name != "Id")
                        {
                            property.SetValue(_entregable, property.GetValue(entregable));
                        }
                    }
                }
                else
                {
                    db.Entregable.Add(entregable);
                }

                db.SaveChanges();
            }
            try
            {

                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder exception = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    string entityError = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Trace.WriteLine(entityError);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        string validationError = string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                            ve.PropertyName,
                                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                            ve.ErrorMessage);
                        entityError += validationError;
                        Trace.WriteLine(validationError);

                    }
                    exception.AppendLine(entityError);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new StringContent(exception.ToString());
                throw new HttpResponseException(resp);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                throw;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created);

            return response;
        }
    }

}
