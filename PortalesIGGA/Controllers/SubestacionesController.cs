﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Subestacion>("Subestaciones");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SubestacionesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Subestaciones
        [EnableQuery]
        public IQueryable<Subestacion> GetSubestaciones()
        {
            return db.Subestacion;
        }

        // GET: odata/Subestaciones(5)
        [EnableQuery]
        public SingleResult<Subestacion> GetSubestacion([FromODataUri] int key)
        {
            return SingleResult.Create(db.Subestacion.Where(subestacion => subestacion.Id == key));
        }

        // PUT: odata/Subestaciones(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Subestacion> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subestacion subestacion = db.Subestacion.Find(key);
            if (subestacion == null)
            {
                return NotFound();
            }

            patch.Put(subestacion);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubestacionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subestacion);
        }

        // POST: odata/Subestaciones
        public IHttpActionResult Post(Subestacion subestacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Subestacion.Add(subestacion);
            db.SaveChanges();

            return Created(subestacion);
        }

        // PATCH: odata/Subestaciones(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Subestacion> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subestacion subestacion = db.Subestacion.Find(key);
            if (subestacion == null)
            {
                return NotFound();
            }

            patch.Patch(subestacion);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubestacionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subestacion);
        }

        // DELETE: odata/Subestaciones(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Subestacion subestacion = db.Subestacion.Find(key);
            if (subestacion == null)
            {
                return NotFound();
            }

            db.Subestacion.Remove(subestacion);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubestacionExists(int key)
        {
            return db.Subestacion.Count(e => e.Id == key) > 0;
        }
    }
}
