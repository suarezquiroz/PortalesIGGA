﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Rol>("Roles");
    builder.EntitySet<RoleByAction>("RoleByAction"); 
    builder.EntitySet<UsuarioContrato>("UsuarioContrato"); 
    builder.EntitySet<UsuarioProyecto>("UsuarioProyecto"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RolesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Roles
        [EnableQuery]
        public IQueryable<Rol> GetRoles()
        {
            return db.Rol;
        }

        // GET: odata/Roles(5)
        [EnableQuery]
        public SingleResult<Rol> GetRol([FromODataUri] int key)
        {
            return SingleResult.Create(db.Rol.Where(rol => rol.Id == key));
        }

        // PUT: odata/Roles(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Rol> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Rol rol = db.Rol.Find(key);
            if (rol == null)
            {
                return NotFound();
            }

            patch.Put(rol);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rol);
        }

        // POST: odata/Roles
        public IHttpActionResult Post(Rol rol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Rol.Add(rol);
            db.SaveChanges();

            return Created(rol);
        }

        // PATCH: odata/Roles(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Rol> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Rol rol = db.Rol.Find(key);
            if (rol == null)
            {
                return NotFound();
            }

            patch.Patch(rol);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(rol);
        }

        // DELETE: odata/Roles(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Rol rol = db.Rol.Find(key);
            if (rol == null)
            {
                return NotFound();
            }

            db.Rol.Remove(rol);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RolExists(int key)
        {
            return db.Rol.Count(e => e.Id == key) > 0;
        }
    }
}
