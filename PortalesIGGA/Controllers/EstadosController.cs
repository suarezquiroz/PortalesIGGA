﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Estado>("Estados");
    builder.EntitySet<Documento>("Documento"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EstadosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Estados
        [EnableQuery]
        public IQueryable<Estado> GetEstados()
        {
            return db.Estado;
        }

        // GET: odata/Estados(5)
        [EnableQuery]
        public SingleResult<Estado> GetEstado([FromODataUri] int key)
        {
            return SingleResult.Create(db.Estado.Where(estado => estado.Id == key));
        }

        // PUT: odata/Estados(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Estado> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estado estado = db.Estado.Find(key);
            if (estado == null)
            {
                return NotFound();
            }

            patch.Put(estado);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(estado);
        }

        // POST: odata/Estados
        public IHttpActionResult Post(Estado estado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Estado.Add(estado);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EstadoExists(estado.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(estado);
        }

        // PATCH: odata/Estados(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Estado> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Estado estado = db.Estado.Find(key);
            if (estado == null)
            {
                return NotFound();
            }

            patch.Patch(estado);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(estado);
        }

        // DELETE: odata/Estados(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Estado estado = db.Estado.Find(key);
            if (estado == null)
            {
                return NotFound();
            }

            db.Estado.Remove(estado);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EstadoExists(int key)
        {
            return db.Estado.Count(e => e.Id == key) > 0;
        }
    }
}
