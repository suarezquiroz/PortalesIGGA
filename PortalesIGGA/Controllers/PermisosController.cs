﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Permiso>("Permisos");
    builder.EntitySet<Accion>("Accion"); 
    builder.EntitySet<Carpeta>("Carpeta"); 
    builder.EntitySet<UsuarioContrato>("UsuarioContrato"); 
    builder.EntitySet<UsuarioProyecto>("UsuarioProyecto"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PermisosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Permisos
        [EnableQuery]
        public IQueryable<Permiso> GetPermisos()
        {
            return db.Permiso;
        }

        // GET: odata/Permisos(5)
        [EnableQuery]
        public SingleResult<Permiso> GetPermiso([FromODataUri] int key)
        {
            return SingleResult.Create(db.Permiso.Where(permiso => permiso.Id == key));
        }

        // PUT: odata/Permisos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Permiso> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Permiso permiso = db.Permiso.Find(key);
            if (permiso == null)
            {
                return NotFound();
            }

            patch.Put(permiso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PermisoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(permiso);
        }

        // POST: odata/Permisos
        public IHttpActionResult Post(Permiso permiso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Permiso.Add(permiso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PermisoExists(permiso.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(permiso);
        }

        // PATCH: odata/Permisos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Permiso> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Permiso permiso = db.Permiso.Find(key);
            if (permiso == null)
            {
                return NotFound();
            }

            patch.Patch(permiso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PermisoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(permiso);
        }

        // DELETE: odata/Permisos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Permiso permiso = db.Permiso.Find(key);
            if (permiso == null)
            {
                return NotFound();
            }

            db.Permiso.Remove(permiso);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PermisoExists(int key)
        {
            return db.Permiso.Count(e => e.Id == key) > 0;
        }
    }
}
