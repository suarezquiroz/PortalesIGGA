﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Subgrupo>("Subgrupos");
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<Grupo>("Grupo"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SubgruposController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Subgrupos
        [EnableQuery]
        public IQueryable<Subgrupo> GetSubgrupos()
        {
            return db.Subgrupo;
        }

        // GET: odata/Subgrupos(5)
        [EnableQuery]
        public SingleResult<Subgrupo> GetSubgrupo([FromODataUri] int key)
        {
            return SingleResult.Create(db.Subgrupo.Where(subgrupo => subgrupo.Id == key));
        }

        // PUT: odata/Subgrupos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Subgrupo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subgrupo subgrupo = db.Subgrupo.Find(key);
            if (subgrupo == null)
            {
                return NotFound();
            }

            patch.Put(subgrupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubgrupoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subgrupo);
        }

        // POST: odata/Subgrupos
        public IHttpActionResult Post(Subgrupo subgrupo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Subgrupo.Add(subgrupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SubgrupoExists(subgrupo.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(subgrupo);
        }

        // PATCH: odata/Subgrupos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Subgrupo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subgrupo subgrupo = db.Subgrupo.Find(key);
            if (subgrupo == null)
            {
                return NotFound();
            }

            patch.Patch(subgrupo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubgrupoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(subgrupo);
        }

        // DELETE: odata/Subgrupos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Subgrupo subgrupo = db.Subgrupo.Find(key);
            if (subgrupo == null)
            {
                return NotFound();
            }

            db.Subgrupo.Remove(subgrupo);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Subgrupos(5)/Grupo
        [EnableQuery]
        public SingleResult<Grupo> GetGrupo([FromODataUri] int key)
        {
            return SingleResult.Create(db.Subgrupo.Where(m => m.Id == key).Select(m => m.Grupo));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubgrupoExists(int key)
        {
            return db.Subgrupo.Count(e => e.Id == key) > 0;
        }
    }
}
