﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;
using System.Text;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using PortalesIGGA.Models;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Web.Http.Results;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/CargarDocumento")]
    public class CargaDocumentosController : ApiController
    {
        private const string Container = "documents";
        private IggaDBEntities db = new IggaDBEntities();
        Documento documentoExistente;

        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> PostFormData()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var accountName = ConfigurationManager.AppSettings["storage:account:name"];
            var accountKey = ConfigurationManager.AppSettings["storage:account:key"];

            TimeSpan backOffPeriod = TimeSpan.FromSeconds(2);
            int retryCount = 1;
            BlobRequestOptions bro = new BlobRequestOptions()
            {
                SingleBlobUploadThresholdInBytes = 1024 * 1024, //1MB, the minimum
                ParallelOperationThreadCount = 1,
                RetryPolicy = new ExponentialRetry(backOffPeriod, retryCount),
            };
            var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            blobClient.DefaultRequestOptions = bro;
            CloudBlobContainer documentsContainer = blobClient.GetContainerReference(Container);
            var provider = new AzureStorageMultipartFormDataStreamProvider(documentsContainer);

            Documento documento;

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);
                string URL = "", nombreArchivo = "";
                //NameValueCollection formData
                string codigoDocumento = provider.FormData.GetValues("CodigoDocumento").FirstOrDefault();
                //check proyecto
                if (!db.Entregable.Any(doc => doc.CodigoDocumento == codigoDocumento))
                {
                    throw new HttpRequestValidationException("No se encontro el documento en el listado de entregables");
                }

                documentoExistente = db.Documento.OrderByDescending(d => d.UltimaVersion).FirstOrDefault(doc => doc.Codigo == codigoDocumento);

                MultipartFileData ArchivoDocumento;
                // This illustrates how to get the file names.
                if (provider.FileData.Count <= 0 || provider.FileData[0] == null)
                {
                    throw new HttpRequestValidationException("no se recibió ningun archivo");
                }

                //foreach (MultipartFileData file in provider.FileData)
                //{
                ArchivoDocumento = provider.FileData[0];
                //nombreArchivo = file.LocalFileName.Split('/').LastOrDefault();
                if (documentoExistente != null && documentoExistente.Id > 0)
                {
                    nombreArchivo = codigoDocumento + "+V" + (Convert.ToInt32(documentoExistente.UltimaVersion) + 1).ToString() + "." + ArchivoDocumento.LocalFileName.Split('.').LastOrDefault();
                }
                else
                {
                    nombreArchivo = codigoDocumento + "+V0." + ArchivoDocumento.LocalFileName.Split('.').LastOrDefault();
                }

                string newFileName = ArchivoDocumento.LocalFileName.Split('/').FirstOrDefault() + "/" + nombreArchivo;

                Trace.WriteLine(ArchivoDocumento.Headers.ContentDisposition.FileName);
                Trace.WriteLine("Server file path: " + ArchivoDocumento.LocalFileName);

                CloudBlockBlob blobCopy = documentsContainer.GetBlockBlobReference(newFileName);

                if (!await blobCopy.ExistsAsync())
                {
                    CloudBlockBlob blob = documentsContainer.GetBlockBlobReference(ArchivoDocumento.LocalFileName);

                    if (await blob.ExistsAsync())
                    {
                        await blobCopy.StartCopyAsync(blob);
                        await blob.DeleteIfExistsAsync();
                    }
                }
                URL = "https://" + accountName + ".blob.core.windows.net/" + Container + "/" + newFileName;
                //}


                foreach (string key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        Trace.WriteLine(string.Format("{0}: {1}", key, val));
                    }

                }

                string categoria = "";
                if (provider.FormData.AllKeys.Contains("Categoria"))
                {
                    categoria = provider.FormData.GetValues("Categoria").FirstOrDefault();
                }

                string contratoId = "0";
                if (provider.FormData.AllKeys.Contains("ContratoId"))
                {
                    contratoId = provider.FormData.GetValues("ContratoId").FirstOrDefault();
                }

                string usuarioId = "0";
                if (provider.FormData.AllKeys.Contains("UsuarioId"))
                {
                    usuarioId = provider.FormData.GetValues("UsuarioId").FirstOrDefault();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
                string descripcion = provider.FormData.GetValues("Descripcion").FirstOrDefault();

                if (codigoDocumento == null || contratoId == null || descripcion == null)
                {
                    HttpResponseException exception = new HttpResponseException(HttpStatusCode.BadRequest);
                    exception.Response.Content = new StringContent(JsonConvert.SerializeObject(provider.FormData, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        }));
                    throw exception;
                }
                documento = GuardarDocumento(contratoId, codigoDocumento, URL, nombreArchivo, descripcion, usuarioId, categoria);

            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            // Retrieve the filename of the file you have uploaded
            var filename = provider.FileData.FirstOrDefault()?.LocalFileName;
            if (string.IsNullOrEmpty(filename))
            {
                //BadRequest("An error has occured while uploading your file. Please try again.");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Ocurrio un error subiendo el archivo, por favor, intentelo de nuevo");
            }

            //registrar en el control de documentos
            ControlDocumentosDt infoControl = new ControlDocumentosDt
            {
                DocumentoNumeroReferenciaCliente = documento.Codigo,
            };
            if (documento.UltimaVersion == "0" || documento.UltimaVersion == null)
            {
                infoControl.Rol = 1;
            }
            else
            {
                infoControl.Rol = 4;
            }
            ControlDocumentController controlDocumentos = new ControlDocumentController();
            var controlResult = controlDocumentos.Control(infoControl);
            CrearTarea(documento);

            var response = Request.CreateResponse(HttpStatusCode.Created, documento);

            return response;
        }

        private Documento GuardarDocumento(string contratoId, string codigoDocumento, string URL, string nombreArchivo, string descripcion, string usuarioId, string categoria)
        {

            Contrato contrato = db.Contrato.FirstOrDefault(c => c.Id.ToString() == contratoId);

            if (contrato == null)
            {
                throw new HttpRequestValidationException("No se encontró el contrato con Id: " + contratoId + " en el sistema");
            }

            Proyecto proyecto = db.Proyecto.FirstOrDefault(p => p.Id == contrato.ProyectoId);

            if (proyecto == null)
            {
                throw new HttpRequestValidationException("No se encontró el proyecto con Id: " + contrato.ProyectoId + " en el sistema");
            }

            Entregable entregable = db.Entregable.FirstOrDefault(ent => ent.CodigoDocumento == codigoDocumento);

            int revision = 0;
            if (documentoExistente != null)
            {
                revision = Convert.ToInt32(documentoExistente.UltimaVersion) + 1;

                //procesar documento existente
                if (revision >= proyecto.CargasPermitidas)
                {
                    throw new HttpRequestValidationException("No se permiten mas cargas para este documento");
                }

            }
            else
            {
                //throw new HttpRequestValidationException("No se encontró el documento '" + codigoDocumento + "' para el proyecto con Id "+proyecto.Id+" en el listado de entregables");
            }

            Grupo _grupo = db.Grupo.FirstOrDefault(g => g.Nombre == entregable.Grupo);
            Subgrupo _subgrupo = db.Subgrupo.FirstOrDefault(sg => sg.Nombre == entregable.Subgrupo);

            Especialidad _especialidad = db.Especialidad.FirstOrDefault(e => e.Nombre == entregable.Especialidad);
            Subespecialidad subespecialidad = db.Subespecialidad.FirstOrDefault(se => se.Nombre == entregable.Subespecialidad);

            //crear nuevo
            Documento _documento = new Documento
            {
                Entregable = entregable,
                Codigo = codigoDocumento,
                URL = URL,
                NombreArchivo = nombreArchivo,
                Descripcion = descripcion,
                Categoria = categoria,

                FechaDisenador = DateTime.Now,
                FechaRecibidoDisenador = DateTime.Now,

                Creado = DateTime.Now,
                CreadoPorId = Convert.ToInt32(usuarioId),
                Modificado = DateTime.Now,
                ModificadoPorId = Convert.ToInt32(usuarioId),

                //change for user
                //Revisión Actual = vacio
                //Revisión = Incrementar el 1 la revisión anterior.Si es nuevo, se debe subir en Revisión “0”.
                UltimaVersion = revision.ToString(),
                //Estado = “En proceso”
                //Tiempo de revisión = 8(depende de los días de revisión)
                NumeroDias = 8,
                Grupo = _grupo,
                Subgrupo = _subgrupo,

                Especialidad = _especialidad,
                Subespecialidad = subespecialidad

            };
            if (contrato != null)
            {
                _documento.Contrato = contrato;
            }
            else
            {
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            Usuario creadoPor = db.Usuario.FirstOrDefault(u => u.Id.ToString() == usuarioId);

            if (creadoPor != null)
            {
                _documento.CreadoPor = creadoPor;
            }
            else
            {
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            Estado estado = db.Estado.Where(e => e.Nombre == "En proceso")?.FirstOrDefault();
            if (estado != null)
            {
                _documento.Estado = estado;
            }
            else
            {
                //throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
            //_documento.CreadoPor = db.UsuarioContrato.Where(u => u.UsuarioId.ToString() == usuarioId && u.ContratoId.ToString() == contratoId)?.FirstOrDefault();

            //_documento.Estado = db.Estado.Where(e => e.Nombre == "En proceso")?.FirstOrDefault();

            db.Documento.Add(_documento);
            db.SaveChanges();

            return _documento;

        }

        public void CrearTarea(Documento documento)
        {
            //insertar tarea
            Tarea _tarea = new Tarea
            {
                DocumentoId = documento.Id,
                Rol = db.Rol.FirstOrDefault(rol => rol.Nombre.ToLower() == "revisor igga"),
                EstadoTarea = db.EstadoTarea.FirstOrDefault(estado => estado.Nombre.ToLower() == "pendiente" && estado.Activo == true)
            };

            db.Tarea.Add(_tarea);

            //try
            //{
            db.SaveChanges();

            string destinatarios = string.Join(";", db.UsuarioContrato.Where(uc => uc.Contrato.Id == documento.Contrato.Id && (uc.Rol.Nombre == "Revisor Restringido" || uc.Rol.Nombre == "Revisor IGGA")).Select(uc => uc.Usuario.Mail).Distinct());
            string nombreDocumento = db.Entregable.FirstOrDefault(e => e.CodigoDocumento == documento.Codigo)?.Descripcion;

            string datosJSON = JsonConvert.SerializeObject(new
            {
                URL = documento.URL,
                //Clasificacion = documento.Clasificacion.Nombre,
                Version = documento.UltimaVersion,
                CodigoDocumento = documento.Codigo,
                NombreDocumento = nombreDocumento
            }
            );
            //enviar notificacion
            Notificacion notificacion = new Notificacion
            {
                IdProyecto = documento.Contrato.Proyecto.Id,
                //6 = correo para revisor
                IdPlantilla = 6,
                //correo de esteban
                IdConfiguracionNotificacion = 4,
                //consultar de usuario contrato
                //Destinatarios = "sasuarez@conocimientocorporativo.com",
                Destinatarios = destinatarios,
                TipoEnvio = "JSON",
                DatosJSON = datosJSON
            };
            string result = notificacion.CrearNotificacion().GetAwaiter().GetResult();

            //}
            //catch (System.Exception e)
            //{
            //    throw e;

            //}
        }

    }
}
