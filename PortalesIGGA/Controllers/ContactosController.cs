﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Contacto>("Contactos");
    builder.EntitySet<Empresa>("Empresa"); 
    builder.EntitySet<Usuario>("Usuario"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ContactosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Contactos
        [EnableQuery]
        public IQueryable<Contacto> GetContactos()
        {
            return db.Contacto;
        }

        // GET: odata/Contactos(5)
        [EnableQuery]
        public SingleResult<Contacto> GetContacto([FromODataUri] int key)
        {
            return SingleResult.Create(db.Contacto.Where(contacto => contacto.Id == key));
        }

        // PUT: odata/Contactos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Contacto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Contacto contacto = db.Contacto.Find(key);
            if (contacto == null)
            {
                return NotFound();
            }

            patch.Put(contacto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(contacto);
        }

        // POST: odata/Contactos
        public IHttpActionResult Post(Contacto contacto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Contacto.Add(contacto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ContactoExists(contacto.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(contacto);
        }

        // PATCH: odata/Contactos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Contacto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Contacto contacto = db.Contacto.Find(key);
            if (contacto == null)
            {
                return NotFound();
            }

            patch.Patch(contacto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(contacto);
        }

        // DELETE: odata/Contactos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Contacto contacto = db.Contacto.Find(key);
            if (contacto == null)
            {
                return NotFound();
            }

            db.Contacto.Remove(contacto);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactoExists(int key)
        {
            return db.Contacto.Count(e => e.Id == key) > 0;
        }
    }
}
