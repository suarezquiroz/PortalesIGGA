﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Especialidad>("Especialidades");
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<Subespecialidad>("Subespecialidad"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EspecialidadesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Especialidades
        [EnableQuery]
        public IQueryable<Especialidad> GetEspecialidades()
        {
            return db.Especialidad;
        }

        // GET: odata/Especialidades(5)
        [EnableQuery]
        public SingleResult<Especialidad> GetEspecialidad([FromODataUri] int key)
        {
            return SingleResult.Create(db.Especialidad.Where(especialidad => especialidad.Id == key));
        }

        // PUT: odata/Especialidades(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Especialidad> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Especialidad especialidad = db.Especialidad.Find(key);
            if (especialidad == null)
            {
                return NotFound();
            }

            patch.Put(especialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EspecialidadExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(especialidad);
        }

        // POST: odata/Especialidades
        public IHttpActionResult Post(Especialidad especialidad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Especialidad.Add(especialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EspecialidadExists(especialidad.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(especialidad);
        }

        // PATCH: odata/Especialidades(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Especialidad> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Especialidad especialidad = db.Especialidad.Find(key);
            if (especialidad == null)
            {
                return NotFound();
            }

            patch.Patch(especialidad);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EspecialidadExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(especialidad);
        }

        // DELETE: odata/Especialidades(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Especialidad especialidad = db.Especialidad.Find(key);
            if (especialidad == null)
            {
                return NotFound();
            }

            db.Especialidad.Remove(especialidad);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EspecialidadExists(int key)
        {
            return db.Especialidad.Count(e => e.Id == key) > 0;
        }
    }
}
