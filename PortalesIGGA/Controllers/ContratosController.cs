﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Contrato>("Contratos");
    builder.EntitySet<Empresa>("Empresa"); 
    builder.EntitySet<Proyecto>("Proyecto"); 
    builder.EntitySet<Documento>("Documento"); 
    builder.EntitySet<UsuarioContrato>("UsuarioContrato"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ContratosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Contratos
        [EnableQuery]
        public IQueryable<Contrato> GetContratos()
        {
            return db.Contrato;
        }

        // GET: odata/Contratos(5)
        [EnableQuery]
        public SingleResult<Contrato> GetContrato([FromODataUri] int key)
        {
            return SingleResult.Create(db.Contrato.Where(contrato => contrato.Id == key));
        }

        // PUT: odata/Contratos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Contrato> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Contrato contrato = db.Contrato.Find(key);
            if (contrato == null)
            {
                return NotFound();
            }

            patch.Put(contrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContratoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(contrato);
        }

        // POST: odata/Contratos
        public IHttpActionResult Post(Contrato contrato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Contrato.Add(contrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ContratoExists(contrato.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(contrato);
        }

        // PATCH: odata/Contratos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Contrato> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Contrato contrato = db.Contrato.Find(key);
            if (contrato == null)
            {
                return NotFound();
            }

            patch.Patch(contrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContratoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(contrato);
        }

        // DELETE: odata/Contratos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Contrato contrato = db.Contrato.Find(key);
            if (contrato == null)
            {
                return NotFound();
            }

            db.Contrato.Remove(contrato);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Contratos(5)/Empresa
        [EnableQuery]
        public SingleResult<Empresa> GetEmpresa([FromODataUri] int key)
        {
            return SingleResult.Create(db.Contrato.Where(m => m.Id == key).Select(m => m.Empresa));
        }

        // GET: odata/Contratos(5)/Proyecto
        [EnableQuery]
        public SingleResult<Proyecto> GetProyecto([FromODataUri] int key)
        {
            return SingleResult.Create(db.Contrato.Where(m => m.Id == key).Select(m => m.Proyecto));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContratoExists(int key)
        {
            return db.Contrato.Count(e => e.Id == key) > 0;
        }
    }
}
