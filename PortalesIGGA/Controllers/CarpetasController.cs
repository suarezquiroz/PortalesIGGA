﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Carpeta>("Carpetas");
    builder.EntitySet<Permiso>("Permiso"); 
    builder.EntitySet<Documento>("Documento"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class CarpetasController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Carpetas
        [EnableQuery]
        public IQueryable<Carpeta> GetCarpetas()
        {
            return db.Carpeta;
        }

        // GET: odata/Carpetas(5)
        [EnableQuery]
        public SingleResult<Carpeta> GetCarpeta([FromODataUri] int key)
        {
            return SingleResult.Create(db.Carpeta.Where(carpeta => carpeta.Id == key));
        }

        // PUT: odata/Carpetas(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Carpeta> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Carpeta carpeta = db.Carpeta.Find(key);
            if (carpeta == null)
            {
                return NotFound();
            }

            patch.Put(carpeta);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarpetaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(carpeta);
        }

        // POST: odata/Carpetas
        public IHttpActionResult Post(Carpeta carpeta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Carpeta.Add(carpeta);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CarpetaExists(carpeta.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(carpeta);
        }

        // PATCH: odata/Carpetas(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Carpeta> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Carpeta carpeta = db.Carpeta.Find(key);
            if (carpeta == null)
            {
                return NotFound();
            }

            patch.Patch(carpeta);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarpetaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(carpeta);
        }

        // DELETE: odata/Carpetas(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Carpeta carpeta = db.Carpeta.Find(key);
            if (carpeta == null)
            {
                return NotFound();
            }

            db.Carpeta.Remove(carpeta);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Carpetas(5)/Permiso
        [EnableQuery]
        public SingleResult<Permiso> GetPermiso([FromODataUri] int key)
        {
            return SingleResult.Create(db.Carpeta.Where(m => m.Id == key).Select(m => m.Permiso));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CarpetaExists(int key)
        {
            return db.Carpeta.Count(e => e.Id == key) > 0;
        }
    }
}
