﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;
using PortalesIGGA.Models;
using System.Data.Entity.Validation;
using System.Text;
using System.Diagnostics;
using System.Web;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/EliminarDocumento")]
    public class EliminarDocumentoController : ApiController
    {
        private IggaDBEntities db = new IggaDBEntities();
        private static Documento documento;

        // POST: api/EliminarDocumento
        [HttpDelete, Route("")]
        public async Task<HttpResponseMessage> Delete(HttpRequestMessage request)
        {
            string jsonString = await request.Content.ReadAsStringAsync();
            try
            {
                EliminacionDocumento eliminacionDocumento = JsonConvert.DeserializeObject<EliminacionDocumento>(jsonString);

                //find document
                if (eliminacionDocumento != null && eliminacionDocumento.DocumentoId > 0)
                {
                    documento = db.Documento.Where(d => d.Id == eliminacionDocumento.DocumentoId).FirstOrDefault();
                }
                else
                {
                    throw new HttpRequestValidationException("Debe proporcionar un Id de documento valido");
                }
                //tareas
                db.Tarea.RemoveRange(db.Tarea.Where(t=>t.DocumentoId == documento.Id));
                db.Tarea.RemoveRange(db.Tarea.Where(t => t.DocumentoId == documento.Id));

                db.Documento.Remove(documento);
                
                db.SaveChanges();
                

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder exception = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    string entityError = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Trace.WriteLine(entityError);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        string validationError = string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                            ve.PropertyName,
                                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                            ve.ErrorMessage);
                        entityError += validationError;
                        Trace.WriteLine(validationError);

                    }
                    exception.AppendLine(entityError);
                }

                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new StringContent(exception.ToString());
                throw new HttpResponseException(resp);

            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                throw;
            }

            //if no exceptions, return newEmpresa
            var response = Request.CreateResponse(HttpStatusCode.NoContent);

            return response;
        }
    }
}

internal class EliminacionDocumento
{
    public int DocumentoId { get; set; }
}