﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Entregable>("Entregables");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EntregablesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Entregables
        [EnableQuery]
        public IQueryable<Entregable> GetEntregables()
        {
            return db.Entregable;
        }

        // GET: odata/Entregables(5)
        [EnableQuery]
        public SingleResult<Entregable> GetEntregable([FromODataUri] int key)
        {
            return SingleResult.Create(db.Entregable.Where(entregable => entregable.Id == key));
        }

        // PUT: odata/Entregables(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Entregable> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Entregable entregable = db.Entregable.Find(key);
            if (entregable == null)
            {
                return NotFound();
            }

            patch.Put(entregable);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntregableExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(entregable);
        }

        // POST: odata/Entregables
        public IHttpActionResult Post(Entregable entregable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entregable.Add(entregable);
            db.SaveChanges();

            return Created(entregable);
        }

        // PATCH: odata/Entregables(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Entregable> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Entregable entregable = db.Entregable.Find(key);
            if (entregable == null)
            {
                return NotFound();
            }

            patch.Patch(entregable);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntregableExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(entregable);
        }

        // DELETE: odata/Entregables(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Entregable entregable = db.Entregable.Find(key);
            if (entregable == null)
            {
                return NotFound();
            }

            db.Entregable.Remove(entregable);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EntregableExists(int key)
        {
            return db.Entregable.Count(e => e.Id == key) > 0;
        }
    }
}
