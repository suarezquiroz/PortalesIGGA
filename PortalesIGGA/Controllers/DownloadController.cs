﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace PortalesIGGA.Controllers
{
    public class DownloadController : ApiController
    {
        public void GetContainerFile(HttpRequest request,HttpResponse response)
        {
            //Retrieve filenname from DB (based on fileid (Guid))
            // *SNIP*
            string filename = "some file name.txt";

            //IE needs URL encoded filename. Important when there are spaces and other non-ansi chars in filename.
            if (HttpContext.Current.Request.UserAgent != null && HttpContext.Current.Request.UserAgent.ToUpper().Contains("MSIE"))
                filename = HttpUtility.UrlEncode(filename, System.Text.Encoding.UTF8).Replace("+", " ");

            HttpContext context = new HttpContext(request, response);
            context.Response.Charset = "UTF-8";
            //Important to set buffer to false. IIS will download entire blob before passing it on to user if this is not set to false
            context.Response.Buffer = false;
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
            context.Response.AddHeader("Content-Length", "100122334"); //Set the length the file
            context.Response.ContentType = "application/octet-stream";
            context.Response.Flush();

            //Use the Azure API to stream the blob to the user instantly.
            // *SNIP*

            var accountName = ConfigurationManager.AppSettings["storage:account:name"];
            var accountKey = ConfigurationManager.AppSettings["storage:account:key"];

            CloudStorageAccount account = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);
            CloudBlobClient blobClient = account.CreateCloudBlobClient();
            CloudBlobContainer documentsContainer = blobClient.GetContainerReference("documents");
            //https://stackoverflow.com/questions/30467013/how-to-download-a-file-to-browser-from-azure-blob-storage
        }
    }
}
