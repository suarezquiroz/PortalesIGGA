﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<UsuarioContrato>("UsuariosContrato");
    builder.EntitySet<Contrato>("Contrato"); 
    builder.EntitySet<Permiso>("Permiso"); 
    builder.EntitySet<Usuario>("Usuario"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class UsuariosContratoController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/UsuariosContrato
        [EnableQuery]
        public IQueryable<UsuarioContrato> GetUsuariosContrato()
        {
            return db.UsuarioContrato;
        }

        // GET: odata/UsuariosContrato(5)
        [EnableQuery]
        public SingleResult<UsuarioContrato> GetUsuarioContrato([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioContrato.Where(usuarioContrato => usuarioContrato.Id == key));
        }

        // PUT: odata/UsuariosContrato(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<UsuarioContrato> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UsuarioContrato usuarioContrato = db.UsuarioContrato.Find(key);
            if (usuarioContrato == null)
            {
                return NotFound();
            }

            patch.Put(usuarioContrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioContratoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usuarioContrato);
        }

        // POST: odata/UsuariosContrato
        public IHttpActionResult Post(UsuarioContrato usuarioContrato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UsuarioContrato.Add(usuarioContrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UsuarioContratoExists(usuarioContrato.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(usuarioContrato);
        }

        // PATCH: odata/UsuariosContrato(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<UsuarioContrato> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UsuarioContrato usuarioContrato = db.UsuarioContrato.Find(key);
            if (usuarioContrato == null)
            {
                return NotFound();
            }

            patch.Patch(usuarioContrato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioContratoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usuarioContrato);
        }

        // DELETE: odata/UsuariosContrato(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            UsuarioContrato usuarioContrato = db.UsuarioContrato.Find(key);
            if (usuarioContrato == null)
            {
                return NotFound();
            }

            db.UsuarioContrato.Remove(usuarioContrato);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/UsuariosContrato(5)/Contrato
        [EnableQuery]
        public SingleResult<Contrato> GetContrato([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioContrato.Where(m => m.Id == key).Select(m => m.Contrato));
        }

        // GET: odata/UsuariosContrato(5)/Usuario
        [EnableQuery]
        public SingleResult<Usuario> GetUsuario([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioContrato.Where(m => m.Id == key).Select(m => m.Usuario));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsuarioContratoExists(int key)
        {
            return db.UsuarioContrato.Count(e => e.Id == key) > 0;
        }
    }
}
