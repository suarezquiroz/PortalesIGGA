﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<DocumentoEspecificacionAnexo>("DocumentosEspecificacionAnexo");
    builder.EntitySet<Proyecto>("Proyecto"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class DocumentosEspecificacionAnexoController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/DocumentosEspecificacionAnexo
        [EnableQuery]
        public IQueryable<DocumentoEspecificacionAnexo> GetDocumentosEspecificacionAnexo()
        {
            return db.DocumentoEspecificacionAnexo;
        }

        // GET: odata/DocumentosEspecificacionAnexo(5)
        [EnableQuery]
        public SingleResult<DocumentoEspecificacionAnexo> GetDocumentoEspecificacionAnexo([FromODataUri] int key)
        {
            return SingleResult.Create(db.DocumentoEspecificacionAnexo.Where(documentoEspecificacionAnexo => documentoEspecificacionAnexo.Id == key));
        }

        // PUT: odata/DocumentosEspecificacionAnexo(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<DocumentoEspecificacionAnexo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DocumentoEspecificacionAnexo documentoEspecificacionAnexo = db.DocumentoEspecificacionAnexo.Find(key);
            if (documentoEspecificacionAnexo == null)
            {
                return NotFound();
            }

            patch.Put(documentoEspecificacionAnexo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoEspecificacionAnexoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(documentoEspecificacionAnexo);
        }

        // POST: odata/DocumentosEspecificacionAnexo
        public IHttpActionResult Post(DocumentoEspecificacionAnexo documentoEspecificacionAnexo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DocumentoEspecificacionAnexo.Add(documentoEspecificacionAnexo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DocumentoEspecificacionAnexoExists(documentoEspecificacionAnexo.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(documentoEspecificacionAnexo);
        }

        // PATCH: odata/DocumentosEspecificacionAnexo(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<DocumentoEspecificacionAnexo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DocumentoEspecificacionAnexo documentoEspecificacionAnexo = db.DocumentoEspecificacionAnexo.Find(key);
            if (documentoEspecificacionAnexo == null)
            {
                return NotFound();
            }

            patch.Patch(documentoEspecificacionAnexo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoEspecificacionAnexoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(documentoEspecificacionAnexo);
        }

        // DELETE: odata/DocumentosEspecificacionAnexo(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            DocumentoEspecificacionAnexo documentoEspecificacionAnexo = db.DocumentoEspecificacionAnexo.Find(key);
            if (documentoEspecificacionAnexo == null)
            {
                return NotFound();
            }

            db.DocumentoEspecificacionAnexo.Remove(documentoEspecificacionAnexo);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/DocumentosEspecificacionAnexo(5)/Proyecto
        [EnableQuery]
        public SingleResult<Proyecto> GetProyecto([FromODataUri] int key)
        {
            return SingleResult.Create(db.DocumentoEspecificacionAnexo.Where(m => m.Id == key).Select(m => m.Proyecto));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocumentoEspecificacionAnexoExists(int key)
        {
            return db.DocumentoEspecificacionAnexo.Count(e => e.Id == key) > 0;
        }
    }
}
