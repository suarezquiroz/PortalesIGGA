﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Accion>("Acciones");
    builder.EntitySet<Permiso>("Permiso"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class AccionesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Acciones
        [EnableQuery]
        public IQueryable<Accion> GetAcciones()
        {
            return db.Accion;
        }

        // GET: odata/Acciones(5)
        [EnableQuery]
        public SingleResult<Accion> GetAccion([FromODataUri] int key)
        {
            return SingleResult.Create(db.Accion.Where(accion => accion.Id == key));
        }

        // PUT: odata/Acciones(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Accion> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Accion accion = db.Accion.Find(key);
            if (accion == null)
            {
                return NotFound();
            }

            patch.Put(accion);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(accion);
        }

        // POST: odata/Acciones
        public IHttpActionResult Post(Accion accion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Accion.Add(accion);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AccionExists(accion.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(accion);
        }

        // PATCH: odata/Acciones(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Accion> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Accion accion = db.Accion.Find(key);
            if (accion == null)
            {
                return NotFound();
            }

            patch.Patch(accion);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(accion);
        }

        // DELETE: odata/Acciones(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Accion accion = db.Accion.Find(key);
            if (accion == null)
            {
                return NotFound();
            }

            db.Accion.Remove(accion);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccionExists(int key)
        {
            return db.Accion.Count(e => e.Id == key) > 0;
        }
    }
}
