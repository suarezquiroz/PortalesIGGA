﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Empresa>("Empresas");
    builder.EntitySet<Contacto>("Contacto"); 
    builder.EntitySet<Contrato>("Contrato"); 
    builder.EntitySet<Pais>("Pais"); 
    builder.EntitySet<Usuario>("Usuario"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EmpresasController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Empresas
        [EnableQuery]
        public IQueryable<Empresa> GetEmpresas()
        {
            return db.Empresa;
        }

        // GET: odata/Empresas(5)
        [EnableQuery]
        public SingleResult<Empresa> GetEmpresa([FromODataUri] int key)
        {
            return SingleResult.Create(db.Empresa.Where(empresa => empresa.Id == key));
        }

        // PUT: odata/Empresas(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Empresa> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Empresa empresa = db.Empresa.Find(key);
            if (empresa == null)
            {
                return NotFound();
            }

            patch.Put(empresa);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpresaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(empresa);
        }

        // POST: odata/Empresas
        public IHttpActionResult Post(Empresa empresa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Empresa.Add(empresa);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EmpresaExists(empresa.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(empresa);
        }

        // PATCH: odata/Empresas(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Empresa> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Empresa empresa = db.Empresa.Find(key);
            if (empresa == null)
            {
                return NotFound();
            }

            patch.Patch(empresa);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpresaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(empresa);
        }

        // DELETE: odata/Empresas(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Empresa empresa = db.Empresa.Find(key);
            if (empresa == null)
            {
                return NotFound();
            }

            db.Empresa.Remove(empresa);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Empresas(5)/Contacto
        [EnableQuery]
        public SingleResult<Contacto> GetContactoPrincipal([FromODataUri] int key)
        {
            return SingleResult.Create(db.Empresa.Where(m => m.Id == key).Select(m => m.ContactoPrincipal));
        }

        // GET: odata/Empresas(5)/Contacto2
        [EnableQuery]
        public SingleResult<Contacto> GetContactoPagos([FromODataUri] int key)
        {
            return SingleResult.Create(db.Empresa.Where(m => m.Id == key).Select(m => m.ContactoPagos));
        }

        // GET: odata/Empresas(5)/Contacto3
        [EnableQuery]
        public SingleResult<Contacto> GetContactoJuridico([FromODataUri] int key)
        {
            return SingleResult.Create(db.Empresa.Where(m => m.Id == key).Select(m => m.ContactoJuridico));
        }

        // GET: odata/Empresas(5)/Pais
        [EnableQuery]
        public SingleResult<Pais> GetPais([FromODataUri] int key)
        {
            return SingleResult.Create(db.Empresa.Where(m => m.Id == key).Select(m => m.Pais));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmpresaExists(int key)
        {
            return db.Empresa.Count(e => e.Id == key) > 0;
        }
    }
}
