﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using PortalesIGGA.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/CompletarTarea")]
    public class CompletarTareaController : ApiController
    {
        // GET: api/CompletarTarea
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/CompletarTarea/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        private const string Container = "documents";
        private IggaDBEntities db = new IggaDBEntities();
        private static Documento DocumentoModelo = new Documento();

        // POST: api/CompletarTarea
        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> PostFormData()
        {
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var accountName = ConfigurationManager.AppSettings["storage:account:name"];
            var accountKey = ConfigurationManager.AppSettings["storage:account:key"];

            var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer documentsContainer = blobClient.GetContainerReference(Container);
            var provider = new AzureStorageMultipartFormDataStreamProvider(documentsContainer);
            Documento _documentoRevision;
            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                string URL = "", nombreArchivo = "";

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    nombreArchivo = file.LocalFileName.Split('/').LastOrDefault();
                    URL = "https://" + accountName + ".blob.core.windows.net/" + Container + "/" + file.LocalFileName;

                    Trace.WriteLine(file.Headers.ContentDisposition.FileName);
                    Trace.WriteLine("Server file path: " + file.LocalFileName);
                }

                foreach (string key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        Trace.WriteLine(string.Format("{0}: {1}", key, val));
                    }
                }
                //procesar form data
                // ClasificacionId: number
                string clasificacionId = provider.FormData.GetValues("ClasificacionId")?.FirstOrDefault();
                //Observacion
                string observacion = provider.FormData.GetValues("Observacion")?.FirstOrDefault();
                //TareaId: number
                string tareaId = provider.FormData.GetValues("TareaId")?.FirstOrDefault();
                // DocumentoId: number,
                string documentoId = provider.FormData.GetValues("DocumentoId")?.FirstOrDefault();
                // UsuarioId: number ,
                string usuarioId = provider.FormData.GetValues("UsuarioId")?.FirstOrDefault();
                // RolId: number(no recuerdo si es posible que un usuario tenga varios roles dentro de un mismo contrato, por eso es mejor enviarlo)
                string rolId = provider.FormData.GetValues("RolId")?.FirstOrDefault();

                Rol rol = db.Rol.FirstOrDefault(r => r.Id.ToString() == rolId);

                //buscar tarea
                Tarea tarea = db.Tarea.FirstOrDefault(t => t.Id.ToString() == tareaId);
                //UsuarioContrato usuarioContrato = db.UsuarioContrato.FirstOrDefault(uc => uc.Usuario.ToString() == usuarioId && uc.RolId.ToString() == rolId);
                tarea.UsuarioCompletacionId = Convert.ToInt32(usuarioId);
                tarea.FechaCompletacion = DateTime.Now;
                tarea.Observacion = observacion;
                tarea.EstadoTarea = db.EstadoTarea.FirstOrDefault(et => et.Nombre == "Completada");
                //actualizar documento
                Documento documento = db.Documento.FirstOrDefault(doc => doc.Id.ToString() == documentoId);
                Clasificacion clasificacion = db.Clasificacion.FirstOrDefault(cl => cl.Id.ToString() == clasificacionId);
                //validar documento.codigo

                _documentoRevision = new Documento
                {
                    Entregable = documento.Entregable,
                    DocumentoPadre = documento.Codigo,
                    URL = URL,
                    NombreArchivo = nombreArchivo,
                    Descripcion = documento.Descripcion,
                    UltimaVersion = documento.UltimaVersion,
                    Clasificacion = clasificacion,
                    CreadoPorId = Convert.ToInt32(usuarioId),
                    Creado = DateTime.Now,
                    Modificado = DateTime.Now,
                    FechaUltimaClasificada = DateTime.Now,

                    //inherit properties
                    NumeroDias = documento.NumeroDias,
                    Contrato = documento.Contrato,
                    Especialidad = documento.Especialidad,
                    Estado = db.Estado.FirstOrDefault(es => es.Nombre == "Enviado"),
                    Grupo = documento.Grupo,
                    Subespecialidad = documento.Subespecialidad,
                    Subgrupo = documento.Subgrupo
                };
                if (nombreArchivo != null)
                {
                    db.Documento.Add(_documentoRevision);
                }
                else if ( rol.Nombre != "Validador")
                {
                    throw new HttpRequestValidationException("Se debe cargar un archivo");
                }

                //actualizar documento
                documento.Clasificacion = clasificacion;
                documento.Modificado = DateTime.Now;
                documento.ModificadoPorId = Convert.ToInt32(usuarioId);


                //registrar en control de documentos
                //enviar notificacion
                //DPC y VCC van al diseñador
                string destinatarios = "";
                //6 revisor, 5 validador,7 diseñador
                int plantillaId = 0;
                //registrar en el control de documentos
                ControlDocumentosDt infoControl = new ControlDocumentosDt
                {
                    DocumentoNumeroReferenciaCliente = documento.Codigo,
                };

                string rolTarea = "";
                if (rol != null)
                {
                    switch (rol.Nombre)
                    {
                        case "Revisor Restringido":
                        case "Revisor IGGA":
                            {

                                documento.FechaRevisor = DateTime.Now;
                                _documentoRevision.FechaRevisor = DateTime.Now;

                                documento.FechaUltimaClasificada = DateTime.Now;

                                documento.FechaClasificacionRevisor = DateTime.Now;
                                documento.ClasificacionRevisor = clasificacion;
                                documento.ObservacionesRevisor = observacion;
                                documento.URLArchivoRevisor = URL;

                                _documentoRevision.FechaClasificacionRevisor = DateTime.Now;
                                _documentoRevision.ClasificacionRevisor = clasificacion;
                                _documentoRevision.ObservacionesRevisor = observacion;
                                _documentoRevision.URLArchivoRevisor = URL;

                                documento.Estado = db.Estado.FirstOrDefault(es => es.Nombre == "Enviado");
                                infoControl.Rol = 2;
                                infoControl.ClasificacionRevisor = clasificacion.Nombre;
                                infoControl.FechaRespuestaRevisor = DateTime.Now;

                                //if (clasificacion.Nombre == "DPC" || clasificacion.Nombre == "VCC")
                                //{
                                //    //devolver a diseñador original
                                //    plantillaId = 7;
                                //    destinatarios = documento.CreadoPor.Mail;
                                //    rolTarea = "Diseñador";
                                //}
                                //else
                                //{

                                //enviar a validadores
                                plantillaId = 5;
                                destinatarios = string.Join(";", db.UsuarioContrato.Where(uc => uc.Contrato.Id == documento.Contrato.Id && uc.Rol.Nombre == "Validador").Select(uc => uc.Usuario.Mail));
                                rolTarea = "Validador";

                                //}
                                break;
                            }
                        case "Validador":
                            {
                                documento.FechaUltimaClasificada = DateTime.Now;
                                documento.Estado = db.Estado.FirstOrDefault(es => es.Nombre == "Enviado");

                                documento.FechaClasificacionValidador = DateTime.Now;
                                documento.ClasificacionValidador = clasificacion;
                                documento.ObservacionesValidador = observacion;
                                documento.URLArchivoValidador = URL;


                                _documentoRevision.FechaClasificacionValidador = DateTime.Now;
                                _documentoRevision.ClasificacionValidador = clasificacion;
                                _documentoRevision.ObservacionesValidador = observacion;
                                _documentoRevision.URLArchivoValidador = URL;

                                _documentoRevision.ClasificacionRevisor = documento.ClasificacionRevisor;
                                _documentoRevision.ObservacionesRevisor = documento.ObservacionesRevisor;
                                _documentoRevision.FechaClasificacionValidador = documento.FechaClasificacionValidador;

                                Documento documentoRevisor = db.Documento.First(doc => doc.DocumentoPadre == documento.Codigo && doc.UltimaVersion == documento.UltimaVersion);

                                if (documentoRevisor != null)
                                {
                                    documentoRevisor.FechaClasificacionValidador = DateTime.Now;
                                    documentoRevisor.ClasificacionValidador = clasificacion;
                                    documentoRevisor.ObservacionesValidador = observacion;
                                }

                                infoControl.Rol = 3;
                                infoControl.ClasificacionActual = clasificacion.Nombre;
                                infoControl.FechaRespuestaValidador = DateTime.Now;
                                infoControl.ClasificacionValidador = clasificacion.Nombre;
                                //siempre devolver a diseñador original
                                plantillaId = 7;
                                destinatarios = documento.CreadoPor.Mail;
                                if (clasificacion.Nombre != "V")
                                {
                                    rolTarea = "Diseñador";
                                }
                                break;
                            }
                        //el diseñador no completa tareas, carga nuevas versiones
                        case "Diseñador":
                        default:
                            //retornar error?
                            break;
                    }
                }

                db.SaveChanges();

                ControlDocumentController controlDocumentos = new ControlDocumentController();
                var controlResult = controlDocumentos.Control(infoControl);

                string nombreDocumento = db.Entregable.FirstOrDefault(e => e.CodigoDocumento == documento.Codigo)?.Descripcion;

                string datosJSON = JsonConvert.SerializeObject(new
                {
                    URL = documento.URL,
                    Clasificacion = documento.Clasificacion.Nombre,
                    CodigoDocumento = documento.Codigo,
                    Version = documento.UltimaVersion,
                    NombreDocumento = nombreDocumento
                }
                );

                //clasificaciones permitidas en fase 1 que generan nueva tarea
                //if (clasificacion.Nombre == "SIN" ||
                //    clasificacion.Nombre == "DPC" ||
                //    clasificacion.Nombre == "VCC")
                if (rolTarea != "")
                {

                    //insertar tarea
                    Tarea _tarea = new Tarea
                    {
                        DocumentoId = documento.Id,
                        Rol = db.Rol.FirstOrDefault(r => r.Nombre == rolTarea),
                        EstadoTarea = db.EstadoTarea.FirstOrDefault(estado => estado.Nombre == "Pendiente" && estado.Activo == true)
                    };
                    db.Tarea.Add(_tarea);
                    db.SaveChanges();

                    //enviar notificacion
                    Notificacion notificacion = new Notificacion
                    {
                        IdProyecto = documento.Contrato.Proyecto.Id,
                        //6 = correo para revisor
                        IdPlantilla = plantillaId,
                        //correo de esteban
                        IdConfiguracionNotificacion = 4,
                        //consultar de usuario contrato
                        //Destinatarios = "sasuarez@conocimientocorporativo.com",
                        Destinatarios = destinatarios,
                        TipoEnvio = "JSON",
                        DatosJSON = datosJSON
                    };

                    string result = notificacion.CrearNotificacion().GetAwaiter().GetResult();
                    Trace.WriteLine(result);
                }

            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                throw;
            }
            var response = Request.CreateResponse(HttpStatusCode.Created, _documentoRevision);

            return response;
        }

    }
}
