﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<EstadoTarea>("EstadosTarea");
    builder.EntitySet<Tarea>("Tarea"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class EstadosTareaController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/EstadosTarea
        [EnableQuery]
        public IQueryable<EstadoTarea> GetEstadosTarea()
        {
            return db.EstadoTarea;
        }

        // GET: odata/EstadosTarea(5)
        [EnableQuery]
        public SingleResult<EstadoTarea> GetEstadoTarea([FromODataUri] int key)
        {
            return SingleResult.Create(db.EstadoTarea.Where(estadoTarea => estadoTarea.Id == key));
        }

        // PUT: odata/EstadosTarea(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<EstadoTarea> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            EstadoTarea estadoTarea = db.EstadoTarea.Find(key);
            if (estadoTarea == null)
            {
                return NotFound();
            }

            patch.Put(estadoTarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoTareaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(estadoTarea);
        }

        // POST: odata/EstadosTarea
        public IHttpActionResult Post(EstadoTarea estadoTarea)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EstadoTarea.Add(estadoTarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EstadoTareaExists(estadoTarea.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(estadoTarea);
        }

        // PATCH: odata/EstadosTarea(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<EstadoTarea> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            EstadoTarea estadoTarea = db.EstadoTarea.Find(key);
            if (estadoTarea == null)
            {
                return NotFound();
            }

            patch.Patch(estadoTarea);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstadoTareaExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(estadoTarea);
        }

        // DELETE: odata/EstadosTarea(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            EstadoTarea estadoTarea = db.EstadoTarea.Find(key);
            if (estadoTarea == null)
            {
                return NotFound();
            }

            db.EstadoTarea.Remove(estadoTarea);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EstadoTareaExists(int key)
        {
            return db.EstadoTarea.Count(e => e.Id == key) > 0;
        }
    }
}
