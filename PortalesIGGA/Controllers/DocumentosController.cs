﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Documento>("Documentos");
    builder.EntitySet<Carpeta>("Carpeta"); 
    builder.EntitySet<Clasificacion>("Clasificacion"); 
    builder.EntitySet<Contrato>("Contrato"); 
    builder.EntitySet<Especialidad>("Especialidad"); 
    builder.EntitySet<Estado>("Estado"); 
    builder.EntitySet<Grupo>("Grupo"); 
    builder.EntitySet<Linea>("Linea"); 
    builder.EntitySet<Subespecialidad>("Subespecialidad"); 
    builder.EntitySet<Subestacion>("Subestacion"); 
    builder.EntitySet<Subgrupo>("Subgrupo"); 
    builder.EntitySet<Tarea>("Tarea"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class DocumentosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Documentos
        [EnableQuery]
        public IQueryable<Documento> GetDocumentos()
        {
            return db.Documento;
        }

        // GET: odata/Documentos(5)
        [EnableQuery]
        public SingleResult<Documento> GetDocumento([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(documento => documento.Id == key));
        }

        // PUT: odata/Documentos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Documento> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Documento documento = db.Documento.Find(key);
            if (documento == null)
            {
                return NotFound();
            }

            patch.Put(documento);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(documento);
        }

        // POST: odata/Documentos
        public IHttpActionResult Post(Documento documento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Documento.Add(documento);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DocumentoExists(documento.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(documento);
        }

        // PATCH: odata/Documentos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Documento> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Documento documento = db.Documento.Find(key);
            if (documento == null)
            {
                return NotFound();
            }

            patch.Patch(documento);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(documento);
        }

        // DELETE: odata/Documentos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Documento documento = db.Documento.Find(key);
            if (documento == null)
            {
                return NotFound();
            }

            db.Documento.Remove(documento);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Documentos(5)/Carpeta
        [EnableQuery]
        public SingleResult<Carpeta> GetCarpeta([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Carpeta));
        }

        // GET: odata/Documentos(5)/Clasificacion
        [EnableQuery]
        public SingleResult<Clasificacion> GetClasificacion([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Clasificacion));
        }

        // GET: odata/Documentos(5)/Contrato
        [EnableQuery]
        public SingleResult<Contrato> GetContrato([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Contrato));
        }

        // GET: odata/Documentos(5)/Especialidad
        [EnableQuery]
        public SingleResult<Especialidad> GetEspecialidad([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Especialidad));
        }

        // GET: odata/Documentos(5)/Estado
        [EnableQuery]
        public SingleResult<Estado> GetEstado([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Estado));
        }

        // GET: odata/Documentos(5)/Grupo
        [EnableQuery]
        public SingleResult<Grupo> GetGrupo([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Grupo));
        }

        // GET: odata/Documentos(5)/Subespecialidad
        [EnableQuery]
        public SingleResult<Subespecialidad> GetSubespecialidad([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Subespecialidad));
        }

        //// GET: odata/Documentos(5)/Subestacion
        //[EnableQuery]
        //public SingleResult<Subestacion> GetSubestacion([FromODataUri] int key)
        //{
        //    return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Subestacion));
        //}

        // GET: odata/Documentos(5)/Subgrupo
        [EnableQuery]
        public SingleResult<Subgrupo> GetSubgrupo([FromODataUri] int key)
        {
            return SingleResult.Create(db.Documento.Where(m => m.Id == key).Select(m => m.Subgrupo));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocumentoExists(int key)
        {
            return db.Documento.Count(e => e.Id == key) > 0;
        }
    }
}
