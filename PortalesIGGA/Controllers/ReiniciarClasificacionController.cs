﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;
using PortalesIGGA.Models;
using System.Data.Entity.Validation;
using System.Text;
using System.Diagnostics;
using System.Web;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/ReiniciarClasificacion")]
    public class ReiniciarClasificacionController : ApiController
    {
        private IggaDBEntities db = new IggaDBEntities();
        private static Documento documento = null;

        // POST: api/ReiniciarClasificacion
        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            string jsonString = await request.Content.ReadAsStringAsync();
            //check content type
            try
            {
                ReinicioClasificacion datosReinicio = JsonConvert.DeserializeObject<ReinicioClasificacion>(jsonString);

                //find document
                if (datosReinicio != null && datosReinicio.DocumentoId > 0)
                {
                    documento = db.Documento.Where(d => d.Id == datosReinicio.DocumentoId).FirstOrDefault();
                }
                else
                {
                    throw new HttpRequestValidationException("Debe proporcionar un Id de documento valido");
                }
                if (documento.Id != datosReinicio.DocumentoId)
                {
                    throw new HttpRequestValidationException("Debe proporcionar un Id de documento valido: " + datosReinicio.DocumentoId);
                }

                //reset data

                documento.Estado = db.Estado.Where(e => e.Nombre == "En proceso")?.FirstOrDefault();

                documento.Clasificacion = null;

                documento.ClasificacionRevisorId = null;
                documento.FechaClasificacionRevisor = null;
                documento.URLArchivoRevisor = null;
                documento.ObservacionesRevisor = null;

                documento.ClasificacionValidadorId = null;
                documento.FechaClasificacionValidador = null;
                documento.ObservacionesValidador = null;
                documento.URLArchivoValidador = null;

                documento.Modificado = documento.Creado;
                documento.ModificadoPorId = documento.CreadoPorId;

                //change for user
                //Revisión Actual = vacio
                //Revisión = Incrementar el 1 la revisión anterior.Si es nuevo, se debe subir en Revisión “0”.
                documento.UltimaVersion = "0";

                db.SaveChanges();

                //delete current tasks
                db.Tarea.RemoveRange(db.Tarea.Where(t => t.DocumentoId == datosReinicio.DocumentoId && t.EstadoTarea.Nombre == "Pendiente").ToList());
                //foreach (Tarea tarea in db.Tarea.Where(t => t.DocumentoId == datosReinicio.DocumentoId && t.EstadoTarea.Nombre == "Pendiente").ToList())
                //{
                    
                //    tarea.EstadoTarea = db.EstadoTarea.FirstOrDefault(et => et.Nombre == "Completada");
                //}
                db.SaveChanges();

                //generate new task
                CargaDocumentosController carga = new CargaDocumentosController();
                carga.CrearTarea(documento);

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder exception = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    string entityError = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Trace.WriteLine(entityError);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        string validationError = string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                            ve.PropertyName,
                                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                            ve.ErrorMessage);
                        entityError += validationError;
                        Trace.WriteLine(validationError);

                    }
                    exception.AppendLine(entityError);
                }

                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new StringContent(exception.ToString());
                throw new HttpResponseException(resp);

            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                throw;
            }

            //if no exceptions, return newEmpresa
            var response = Request.CreateResponse(HttpStatusCode.OK, db.Documento.SingleOrDefault(e => e.Id == documento.Id));

            return response;
        }
    }
}

internal class ReinicioClasificacion
{
    public int DocumentoId { get; set; }
}