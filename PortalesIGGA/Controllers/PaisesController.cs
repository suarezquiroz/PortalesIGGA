﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Pais>("Paises");
    builder.EntitySet<Empresa>("Empresa"); 
    builder.EntitySet<Proyecto>("Proyecto"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class PaisesController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Paises
        [EnableQuery]
        public IQueryable<Pais> GetPaises()
        {
            return db.Pais;
        }

        // GET: odata/Paises(5)
        [EnableQuery]
        public SingleResult<Pais> GetPais([FromODataUri] int key)
        {
            return SingleResult.Create(db.Pais.Where(pais => pais.Id == key));
        }

        // PUT: odata/Paises(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Pais> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pais pais = db.Pais.Find(key);
            if (pais == null)
            {
                return NotFound();
            }

            patch.Put(pais);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaisExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pais);
        }

        // POST: odata/Paises
        public IHttpActionResult Post(Pais pais)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pais.Add(pais);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PaisExists(pais.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(pais);
        }

        // PATCH: odata/Paises(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Pais> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pais pais = db.Pais.Find(key);
            if (pais == null)
            {
                return NotFound();
            }

            patch.Patch(pais);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaisExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(pais);
        }

        // DELETE: odata/Paises(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Pais pais = db.Pais.Find(key);
            if (pais == null)
            {
                return NotFound();
            }

            db.Pais.Remove(pais);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaisExists(int key)
        {
            return db.Pais.Count(e => e.Id == key) > 0;
        }
    }
}
