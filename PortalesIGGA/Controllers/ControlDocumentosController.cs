﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ControlDocumento>("ControlDocumentos");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ControlDocumentosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/ControlDocumentos
        [EnableQuery]
        public IQueryable<ControlDocumento> GetControlDocumentos()
        {
            return db.ControlDocumento;
        }

        // GET: odata/ControlDocumentos(5)
        [EnableQuery]
        public SingleResult<ControlDocumento> GetControlDocumento([FromODataUri] int key)
        {
            return SingleResult.Create(db.ControlDocumento.Where(controlDocumento => controlDocumento.Id == key));
        }

        // PUT: odata/ControlDocumentos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<ControlDocumento> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ControlDocumento controlDocumento = db.ControlDocumento.Find(key);
            if (controlDocumento == null)
            {
                return NotFound();
            }

            patch.Put(controlDocumento);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ControlDocumentoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(controlDocumento);
        }

        // POST: odata/ControlDocumentos
        public IHttpActionResult Post(ControlDocumento controlDocumento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ControlDocumento.Add(controlDocumento);
            db.SaveChanges();

            return Created(controlDocumento);
        }

        // PATCH: odata/ControlDocumentos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<ControlDocumento> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ControlDocumento controlDocumento = db.ControlDocumento.Find(key);
            if (controlDocumento == null)
            {
                return NotFound();
            }

            patch.Patch(controlDocumento);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ControlDocumentoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(controlDocumento);
        }

        // DELETE: odata/ControlDocumentos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            ControlDocumento controlDocumento = db.ControlDocumento.Find(key);
            if (controlDocumento == null)
            {
                return NotFound();
            }

            db.ControlDocumento.Remove(controlDocumento);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ControlDocumentoExists(int key)
        {
            return db.ControlDocumento.Count(e => e.Id == key) > 0;
        }
    }
}
