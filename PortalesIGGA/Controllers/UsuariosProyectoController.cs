﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<UsuarioProyecto>("UsuariosProyecto");
    builder.EntitySet<Permiso>("Permiso"); 
    builder.EntitySet<Proyecto>("Proyecto"); 
    builder.EntitySet<Usuario>("Usuario"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class UsuariosProyectoController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/UsuariosProyecto
        [EnableQuery]
        public IQueryable<UsuarioProyecto> GetUsuariosProyecto()
        {
            return db.UsuarioProyecto;
        }

        // GET: odata/UsuariosProyecto(5)
        [EnableQuery]
        public SingleResult<UsuarioProyecto> GetUsuarioProyecto([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioProyecto.Where(usuarioProyecto => usuarioProyecto.Id == key));
        }

        // PUT: odata/UsuariosProyecto(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<UsuarioProyecto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UsuarioProyecto usuarioProyecto = db.UsuarioProyecto.Find(key);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }

            patch.Put(usuarioProyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioProyectoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usuarioProyecto);
        }

        // POST: odata/UsuariosProyecto
        public IHttpActionResult Post(UsuarioProyecto usuarioProyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UsuarioProyecto.Add(usuarioProyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UsuarioProyectoExists(usuarioProyecto.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(usuarioProyecto);
        }

        // PATCH: odata/UsuariosProyecto(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<UsuarioProyecto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            UsuarioProyecto usuarioProyecto = db.UsuarioProyecto.Find(key);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }

            patch.Patch(usuarioProyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioProyectoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usuarioProyecto);
        }

        // DELETE: odata/UsuariosProyecto(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            UsuarioProyecto usuarioProyecto = db.UsuarioProyecto.Find(key);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }

            db.UsuarioProyecto.Remove(usuarioProyecto);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // GET: odata/UsuariosProyecto(5)/Proyecto
        [EnableQuery]
        public SingleResult<Proyecto> GetProyecto([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioProyecto.Where(m => m.Id == key).Select(m => m.Proyecto));
        }

        // GET: odata/UsuariosProyecto(5)/Usuario
        [EnableQuery]
        public SingleResult<Usuario> GetUsuario([FromODataUri] int key)
        {
            return SingleResult.Create(db.UsuarioProyecto.Where(m => m.Id == key).Select(m => m.Usuario));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsuarioProyectoExists(int key)
        {
            return db.UsuarioProyecto.Count(e => e.Id == key) > 0;
        }
    }
}
