﻿using Newtonsoft.Json;
using PortalesIGGA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace PortalesIGGA.Controllers
{
    [RoutePrefix("api/CrearEmpresa")]
    public class CrearEmpresaController : ApiController
    {
        private IggaDBEntities db = new IggaDBEntities();
        private Empresa nuevaEmpresa;

        // POST: api/CrearEmpresa
        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            string jsonString = await request.Content.ReadAsStringAsync();

            try
            {

                NuevaEmpresa datosEmpresa = JsonConvert.DeserializeObject<NuevaEmpresa>(jsonString);

                nuevaEmpresa = new Empresa
                {

                    Codigo = datosEmpresa.Codigo,

                    NIT = datosEmpresa.NIT,

                    Ciudad = datosEmpresa.Ciudad,

                    PaisId = datosEmpresa.PaisId,

                    RazonSocial = datosEmpresa.RazonSocial,

                    NombreComercial = datosEmpresa.NombreComercial,

                    DetalleIngenieria = datosEmpresa.DetalleIngenieria,

                    DireccionPrincipal = datosEmpresa.DireccionPrincipal,

                    DireccionCorrespondencia = datosEmpresa.DireccionCorrespondencia,

                    TelefonoPrincipal = datosEmpresa.TelefonoPrincipal

                };
                Empresa empresaCreada = db.Empresa.Add(nuevaEmpresa);
                db.SaveChanges();

                //Create contacts

                Contacto contactoPrincipal = new Contacto
                {
                    Nombre = datosEmpresa.NombreContactoPrincipal,

                    Telefono = datosEmpresa.TelefonoContactoPrincipal,

                    Celular = datosEmpresa.CelularContactoPrincipal,

                    Mail = datosEmpresa.MailContactoPrincipal,

                    Cargo ="Contacto Principal",

                    EmpresaId = empresaCreada.Id
                };

                db.Contacto.Add(contactoPrincipal);


                Contacto contactoJuridico = new Contacto
                {
                    Nombre = datosEmpresa.NombreContactoJuridico,

                    Telefono = datosEmpresa.TelefonoContactoJuridico,

                    Celular = datosEmpresa.CelularContactoJuridico,

                    Mail = datosEmpresa.MailContactoJuridico,

                    Cargo = "Contacto Juridico",

                    EmpresaId = empresaCreada.Id
                };

                db.Contacto.Add(contactoJuridico);


                Contacto contactoPagos = new Contacto
                {
                    Nombre = datosEmpresa.NombreContactoPagos,

                    Telefono = datosEmpresa.TelefonoContactoPagos,

                    Celular = datosEmpresa.CelularContactoPagos,

                    Mail = datosEmpresa.MailContactoPagos,

                    Cargo = "Contacto Pagos",

                    EmpresaId = empresaCreada.Id
                };

                db.Contacto.Add(contactoPagos);


                db.SaveChanges();

                empresaCreada.ContactoPrincipal = contactoPrincipal;
                empresaCreada.ContactoJuridico = contactoJuridico;
                empresaCreada.ContactoPagos = contactoPagos;
                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder exception = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    string entityError = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    Trace.WriteLine(entityError);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        string validationError = string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                            ve.PropertyName,
                                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                            ve.ErrorMessage);
                        entityError += validationError;
                        Trace.WriteLine(validationError);

                    }
                    exception.AppendLine(entityError);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
                resp.Content = new StringContent(exception.ToString());
                throw new HttpResponseException(resp);
            }
            catch (System.Exception e)
            {
                if (e is DbEntityValidationException)
                {

                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
                throw;
            }
            
            //if no exceptions, return newEmpresa
            var response = Request.CreateResponse(HttpStatusCode.Created, db.Empresa.SingleOrDefault(e=>e.Id == nuevaEmpresa.Id));

            return response;
        }

    }
}


internal class NuevaEmpresa
{

    public string Codigo { get; set; }

    public string NIT { get; set; }

    public string Ciudad { get; set; }

    public int PaisId { get; set; }

    public string RazonSocial { get; set; }

    public string NombreComercial { get; set; }

    public string DetalleIngenieria { get; set; }

    public string DireccionPrincipal { get; set; }

    public string DireccionCorrespondencia { get; set; }

    public string TelefonoPrincipal { get; set; }

    public string NombreContactoPrincipal { get; set; }

    public string TelefonoContactoPrincipal { get; set; }

    public string CelularContactoPrincipal { get; set; }

    public string MailContactoPrincipal { get; set; }

    public string NombreContactoPagos { get; set; }

    public string TelefonoContactoPagos { get; set; }

    public string CelularContactoPagos { get; set; }

    public string MailContactoPagos { get; set; }

    public string NombreContactoJuridico { get; set; }

    public string TelefonoContactoJuridico { get; set; }

    public string CelularContactoJuridico { get; set; }

    public string MailContactoJuridico { get; set; }

}