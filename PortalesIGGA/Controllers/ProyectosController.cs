﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Proyecto>("Proyectos");
    builder.EntitySet<Contrato>("Contrato"); 
    builder.EntitySet<DocumentoEspecificacionAnexo>("DocumentoEspecificacionAnexo"); 
    builder.EntitySet<Pais>("Pais"); 
    builder.EntitySet<UsuarioProyecto>("UsuarioProyecto"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProyectosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Proyectos
        [EnableQuery]
        public IQueryable<Proyecto> GetProyectos()
        {
            return db.Proyecto;
        }

        // GET: odata/Proyectos(5)
        [EnableQuery]
        public SingleResult<Proyecto> GetProyecto([FromODataUri] int key)
        {
            return SingleResult.Create(db.Proyecto.Where(proyecto => proyecto.Id == key));
        }

        // PUT: odata/Proyectos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Proyecto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Proyecto proyecto = db.Proyecto.Find(key);
            if (proyecto == null)
            {
                return NotFound();
            }

            patch.Put(proyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProyectoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(proyecto);
        }

        // POST: odata/Proyectos
        public IHttpActionResult Post(Proyecto proyecto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Proyecto.Add(proyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ProyectoExists(proyecto.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(proyecto);
        }

        // PATCH: odata/Proyectos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Proyecto> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Proyecto proyecto = db.Proyecto.Find(key);
            if (proyecto == null)
            {
                return NotFound();
            }

            patch.Patch(proyecto);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProyectoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(proyecto);
        }

        // DELETE: odata/Proyectos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Proyecto proyecto = db.Proyecto.Find(key);
            if (proyecto == null)
            {
                return NotFound();
            }

            db.Proyecto.Remove(proyecto);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Proyectos(5)/Pais
        [EnableQuery]
        public SingleResult<Pais> GetPais([FromODataUri] int key)
        {
            return SingleResult.Create(db.Proyecto.Where(m => m.Id == key).Select(m => m.Pais));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProyectoExists(int key)
        {
            return db.Proyecto.Count(e => e.Id == key) > 0;
        }
    }
}
