﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using PortalesIGGA.Models;

namespace PortalesIGGA.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using PortalesIGGA.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Recurso>("Recursos");
    builder.EntitySet<Permiso>("Permiso"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RecursosController : ODataController
    {
        private IggaDBEntities db = new IggaDBEntities();

        // GET: odata/Recursos
        [EnableQuery]
        public IQueryable<Recurso> GetRecursos()
        {
            return db.Recurso;
        }

        // GET: odata/Recursos(5)
        [EnableQuery]
        public SingleResult<Recurso> GetRecurso([FromODataUri] int key)
        {
            return SingleResult.Create(db.Recurso.Where(recurso => recurso.IdRecurso == key));
        }

        // PUT: odata/Recursos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Recurso> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Recurso recurso = db.Recurso.Find(key);
            if (recurso == null)
            {
                return NotFound();
            }

            patch.Put(recurso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecursoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(recurso);
        }

        // POST: odata/Recursos
        public IHttpActionResult Post(Recurso recurso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Recurso.Add(recurso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (RecursoExists(recurso.IdRecurso))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(recurso);
        }

        // PATCH: odata/Recursos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Recurso> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Recurso recurso = db.Recurso.Find(key);
            if (recurso == null)
            {
                return NotFound();
            }

            patch.Patch(recurso);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecursoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(recurso);
        }

        // DELETE: odata/Recursos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Recurso recurso = db.Recurso.Find(key);
            if (recurso == null)
            {
                return NotFound();
            }

            db.Recurso.Remove(recurso);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Recursos(5)/Permiso
        //[EnableQuery]
        //public IQueryable<Permiso> GetPermiso([FromODataUri] int key)
        //{
        //    return db.Recurso.Where(m => m.IdRecurso == key).SelectMany(m => m.Permiso);
        //}
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RecursoExists(int key)
        {
            return db.Recurso.Count(e => e.IdRecurso == key) > 0;
        }
    }
}
